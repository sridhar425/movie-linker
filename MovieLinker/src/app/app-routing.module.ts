import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { MoviesComponent } from './components/movies/movies.component';
import { TvshowsComponent } from './components/tvshows/tvshows.component';
import { TrailersComponent } from './components/trailers/trailers.component';
import { DocumentariesComponent } from './components/documentaries/documentaries.component';
import { DocumentaryPageComponent } from './components/documentary-page/documentary-page.component';
import { AnimeComponent } from './components/anime/anime.component';
import { SongsComponent } from './components/songs/songs.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { MoviePageComponent } from './components/movie-page/movie-page.component';
import { TvshowPageComponent } from './components/tvshow-page/tvshow-page.component';
import { TvshowEpisodePageComponent } from './components/tvshow-episode-page/tvshow-episode-page.component';
import { MoviePartPageComponent } from './components/movie-part-page/movie-part-page.component';
import { ProfileComponent } from './components/profile/profile.component';
import { FilterComponent } from './components/filter/filter.component';
import { UploadSectionComponent } from './components/upload-section/upload-section.component';
import { RequestLinkComponent } from './components/request-link/request-link.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AnimeEpisodeListComponent } from './components/anime-episode-list/anime-episode-list.component';
import { AnimeEpisodePageComponent } from './components/anime-episode-page/anime-episode-page.component';
import { SongPageComponent } from './components/song-page/song-page.component';
import { TrailerPageComponent } from './components/trailer-page/trailer-page.component';
import { RequestsComponent } from './components/requests/requests.component';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent},
  { path: 'movies', component: MoviesComponent},
  { path: 'movies/:id', component: MoviePageComponent},
  { path: 'movies/:id/:id', component: MoviePartPageComponent},
  { path: 'tvshows', component: TvshowsComponent},
  { path: 'tvshows/:id', component: TvshowPageComponent},
  { path: 'tvshows/:id/:string', component: TvshowEpisodePageComponent},
  { path: 'trailers', component: TrailersComponent},
  { path: 'trailers/:id', component: TrailerPageComponent},
  { path: 'documentaries', component: DocumentariesComponent},
  { path: 'documentaries/:id', component: DocumentaryPageComponent},
  { path: 'anime', component: AnimeComponent},
  { path: 'anime/:id', component: AnimeEpisodeListComponent},
  { path: 'anime/:id/:string', component: AnimeEpisodePageComponent},
  { path: 'songs', component: SongsComponent},
  { path: 'songs/:id', component: SongPageComponent},
  { path: 'filter', component: FilterComponent},
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
  { path: 'upload-link', component: UploadSectionComponent, canActivate: [AuthGuard]},
  { path: 'request-link', component: RequestLinkComponent, canActivate: [AuthGuard]},
  { path: 'requests', component: RequestsComponent},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: '**', component: PageNotFoundComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  HomeComponent,
  MoviesComponent,
  MoviePageComponent,
  MoviePartPageComponent,
  TvshowsComponent,
  TvshowPageComponent,
  TvshowEpisodePageComponent,
  TrailersComponent,
  DocumentariesComponent,
  DocumentaryPageComponent,
  AnimeComponent,
  SongsComponent,
  PageNotFoundComponent,
  FilterComponent,
  UploadSectionComponent,
  LoginComponent,
  RegisterComponent,
  ProfileComponent,
  AnimeEpisodeListComponent,
  AnimeEpisodePageComponent,
  RequestLinkComponent,
  SongPageComponent,
  TrailerPageComponent,
  RequestsComponent
];
