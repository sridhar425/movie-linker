export class ShowType {
    public showObj; //Global show data passing object;
    show(value) {
        if (value == 'defaultOption') {
            this.showObj = {
                // no needed for movie
                genre: '',
                language: '',
                name: '',
                partOrSeason: '',
                image: '',
                links: '',
                episode: '', episodeLabel: '',
                singer: '', singerLabel: '',
                youtubeLink: '', youtubeLinkLabel: '',
                trailerType: [],
                linkType: ""
            }
            return this.showObj;
        } else if (value == 'movie') {
            this.showObj = {
                genre: 'movieGenre', genreLabel: 'Genre', genrePlaceholder: 'Ex. Action, Thriller',
                language: 'movieLang', languageLabel: 'Language', langPlaceholder: 'Ex English, French, Japanees, Hindi',
                name: 'movieName', nameLabel: 'Name', namePlaceholder: 'Ex. Avatar, Black panther',
                partOrSeason: 'moviePart', partOrSeasonLabel: 'Part / Subname', partOrSeasonPlaceholder: 'Ex. Part 1, Part 2, The Dark Rises, 2001 ',
                image: 'image', imageLabel: 'Uplad Poster Image',
                links: 'links', linksLabel: 'Links', linksPlaceholder: 'movie Links (one by one)',
                youtubeLink: '', youtubeLinkLabel: 'Enter Youtube Link', youtubeLinkPlaceholder: 'youtube Links',
                linkType: "checkbox",
                // no needed for movie
                episode: '', episodeLabel: '',
                singer: '', singerLabel: '',
                trailerType: []
            }
            return this.showObj;
        } else if (value == 'tv_show') {
            this.showObj = {
                genre: 'tvShowGenre', genreLabel: 'Genre', genrePlaceholder: 'Ex. Action, Thriller',
                language: 'tvshowLang', languageLabel: 'Language', langPlaceholder: 'Ex English, French, Japanees, Hindi',
                name: 'tvShowName', nameLabel: ' Name', namePlaceholder: 'Ex. Flash, Game of Thromes',
                partOrSeason: 'tvShowSeason', partOrSeasonLabel: 'Season', partOrSeasonPlaceholder: 'Ex. Season 1, se1',
                episode: 'tvShowEpisode', episodeLabel: 'Episode', episodePlaceholder: 'Ex episode 1, ep1',
                image: 'image', imageLabel: 'Tv Show Image',
                links: 'links', linksLabel: 'Links', linksPlaceholder: 'episode Links (one by one)',
                youtubeLink: '', youtubeLinkLabel: 'Enter Youtube Link', youtubeLinkPlaceholder: 'youtube Links',
                linkType: "checkbox",
                // no needed for tv show
                singer: '', singerLabel: '',
                trailerType: []
            }
            return this.showObj;
        } else if (value == 'documentary') {
            this.showObj = {
                genre: 'documentaryGenre', genreLabel: 'Genre', genrePlaceholder: 'Ex. Action, Thriller',
                language: 'documLang', languageLabel: 'Language', langPlaceholder: 'Ex English, French, Japanees, Hindi',
                name: 'documentaryName', nameLabel: 'Name', namePlaceholder: 'Ex.  Marilyn ,  Inside Burma',
                partOrSeason: 'documentaryPart', partOrSeasonLabel: 'Part / Subname', partOrSeasonPlaceholder: 'Ex. Part 1, Land of Fear, 2001',
                image: 'image', imageLabel: 'Documentary Image',
                links: 'links', linksLabel: 'Links', linksPlaceholder: 'documentary Links (one by one)',
                youtubeLink: '', youtubeLinkLabel: 'Enter Youtube Link', youtubeLinkPlaceholder: 'youtube Links',
                linkType: "checkbox",
                // no needed for documentary
                episode: '', episodeLabel: '',
                singer: '', singerLabel: '',
                trailerType: []
            }
            return this.showObj;
        } else if (value == 'anime') {
            this.showObj = {
                genre: 'animeGenre', genreLabel: 'Genre', genrePlaceholder: 'Ex. Action, Thriller',
                language: 'animeLang', languageLabel: 'Language', langPlaceholder: 'Ex English, French, Japanees, Hindi',
                name: 'animeName', nameLabel: 'Name', namePlaceholder: 'Ex. Naruto, Ben10',
                partOrSeason: 'animeSeason', partOrSeasonLabel: 'Season', partOrSeasonPlaceholder: 'Ex. Season 1, se1',
                episode: 'animeEpisode', episodeLabel: 'Episode', episodePlaceholder: 'Ex episode 1, ep1',
                image: 'image', imageLabel: 'Anime Image',
                links: 'links', linksLabel: 'Links', linksPlaceholder: 'episode Links (one by one)',
                youtubeLink: '', youtubeLinkLabel: 'Enter Youtube Link', youtubeLinkPlaceholder: 'youtube Links',
                linkType: "checkbox",
                // no needed for anime
                singer: '', singerLabel: '',
                trailerType: []
            }
            return this.showObj;
        } else if (value == 'songs') {
            this.showObj = {
                genre: 'songGenre', genreLabel: 'Genre', genrePlaceholder: 'Ex. Pop, Melody, Folk',
                language: 'songLang', languageLabel: 'Language', langPlaceholder: 'Ex English, French, Japanees, Hindi',
                name: 'songName', nameLabel: 'Name', namePlaceholder: 'Ex. Shap of You, Despacito',
                singer: 'songSinger', singerLabel: 'Singer Name', singerPlaceholder: 'Ex. Justin Bieber, Lady gaga',
                youtubeLink: '', youtubeLinkLabel: 'Enter Youtube Link', youtubeLinkPlaceholder: 'youtube Links',
                image: 'image', imageLabel: 'Song Image',
                links: 'links', linksLabel: 'Links', linksPlaceholder: 'song Link (one by one)',
                linkType: "checkbox",
                partOrSeason: '', partOrSeasonLabel: '',
                episode: '', episodeLabel: '',
                trailerType: [],
            }
            return this.showObj;
        } else if (value == 'trailer') {
            this.showObj = {
                trailerType: [
                    { name: 'Movie', value: 'movie' },
                    { name: 'TV Show', value: 'tv_show' },
                    { name: 'Documentary', value: 'documentary' },
                    { name: 'Anime', value: 'anime' }
                ], trailerTypeLabel: 'Trailer Type',
                genre: 'trailerGenre', genreLabel: 'Genre', genrePlaceholder: 'Ex. Action, Thriller',
                language: 'trailerLang', languageLabel: 'Language', langPlaceholder: 'Ex English, French, Japanees, Hindi',
                name: 'trailerName', nameLabel: 'Name', namePlaceholder: 'Ex. Avatar, Black panther',
                partOrSeason: 'trailerSeasonOrPart', partOrSeasonLabel: 'Part / Subname / Season', partOrSeasonPlaceholder: 'Ex. Season 1, se1, Part 2, The Dark Rises, 2001',
                episode: 'trailerEpisode', episodeLabel: 'Episode', episodePlaceholder: 'Ex episode 1, ep1',
                youtubeLink: '', youtubeLinkLabel: 'Enter Youtube Link', youtubeLinkPlaceholder: 'youtube Links',
                image: 'image', imageLabel: 'Trailer Image',
                links: 'links', linksLabel: 'Links', linksPlaceholder: 'trailer Links (one by one)',
                linkType: "checkbox",
                // no needed for anime
                singer: '', singerLabel: '',
            }
            return this.showObj;
        }
    }
} 