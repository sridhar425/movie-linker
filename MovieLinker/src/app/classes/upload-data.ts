
export class UploadData {
    constructor(
        public showType: string,
        public language: string,
        public genre: string,
        public name: string,
        public singer: string,
        public seasonOrPart: string,
        public episode: string,
        public youtubeLinks: string,
        public image: String,
        public links: any,
        public trailerType: string,
        public status: Number,
        public linkType: String
    ) {}
}
