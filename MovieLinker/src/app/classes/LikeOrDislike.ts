import { AuthService } from '../services/auth.service';
import { ShowsService } from '../services/shows.service';
import { Router } from '@angular/router';

export class LikeOrDislike {
    userData: any;
    voteData: any = [];

    ngOnInit(){
        this._auth.isLoginAuth().subscribe( data => { //to get user login
            this.userData = data
        })
    }

    constructor( 
      public _show: ShowsService, 
      public _auth: AuthService,
      public _route: Router
    ){}

    voting(value1, value2, value3, showId, type) { //LIKE OR DISLIKE THE LINK
      if (this.userData.status === true) {
        let voteProp = {
          vote: parseInt(value1),
          show_id: showId,
          link_id: value2
        }
        let voteForColor = document.getElementById(value2).querySelector('button i.' + value3) as HTMLButtonElement; // id > button > i Selector
        this._show.showVote(voteProp) //ADD LIKE OR DISLIKE TO DB
          .subscribe(
            data => {
              if (data.status != 500) {
                let arryOfClasses = voteForColor.className.split(" ");
                this._show.getSingleShow(showId, type).subscribe( // get single show row
                    result => {
                    this.voteData = result
                    let forToCountVote = this.voteData[0].linkTable.filter(ele => ele.id === voteProp.link_id)[0].voteTable;
                    let countBindLike = (document.getElementById(value2).querySelector('span.like'))
                    let countBindDislike = (document.getElementById(value2).querySelector('span.dislike'))
                    if (data.response != 'deleted') {
                      arryOfClasses.find(ele => {
                        if (ele == 'like') { // Remove dislike and put like with total count
                          let removeDangerColor = (document.getElementById(value2).querySelector('button i.dislike') as HTMLButtonElement);
                          removeDangerColor.classList.remove('text-danger');
                          voteForColor.classList.add('text-success');
                          countBindLike.textContent = this.count(forToCountVote, 1);
                          countBindDislike.textContent = this.count(forToCountVote, -1);
                          return true;
                        } else if (ele == 'dislike') { // Remove like and put dislike with total count
                          let removeSuccessColor = (document.getElementById(value2).querySelector('button i.like') as HTMLButtonElement);
                          removeSuccessColor.classList.remove('text-success');
                          voteForColor.classList.add('text-danger');
                          countBindLike.textContent = this.count(forToCountVote, 1);
                          countBindDislike.textContent = this.count(forToCountVote, -1);
                          return true;
                        }
                      })
                    } else {
                      voteForColor.classList.remove('text-success');
                      voteForColor.classList.remove('text-danger');
                      countBindLike.textContent = this.count(forToCountVote, 1);
                      countBindDislike.textContent = this.count(forToCountVote, -1);
                    }
                  },
                  error => console.log(error)
                )
              } else {
                alert('Something went wrong!')
              }
            },
            error => console.log(error)
          )
      } else {
        this._route.navigate(['login'])
      }
      
    }

    count(vote: any, type: Number){ //COUNT VOTES
      let voteObj = vote.map(data => {
        return data.vote
      })
      return voteObj.filter(ele => ele == type).length;
    }
    
    checkVote(value, type, id){ // TO CHECK INDIVIDUAL VOTE WHEN PAGE REFRESHED IF THEY LOGED IN.
        let forColor = value.filter( ele => {
          if (ele.created_by === this.userData['user'] && type === ele.vote) {
            return ele;
          }
        });
        if(forColor.length !=0 ){
          if(forColor[0].vote == 1){
              return 'text-success'
          } 
          if(forColor[0].vote == -1){
              return 'text-danger'
          }
        }
    }
}