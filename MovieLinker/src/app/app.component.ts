import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isLoggedIn: boolean = false;

  constructor(private _auth: AuthService){}

  ngOnInit(){
    this._auth.isLoginAuth()
      .subscribe(
        (data) => {
          console.log(data)
          if(data.status == true){
            this.isLoggedIn = true;
          }
        }
      )
  }
  signout(){
    this._auth.logout()
    .subscribe(
      (data) => {
        console.log(data)
        if(data.status == true){
          window.location.href = '/login';
        }
      }
    )
  }
}
