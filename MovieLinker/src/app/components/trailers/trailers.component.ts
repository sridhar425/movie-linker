import { Component, OnInit } from '@angular/core';
import { ShowsService } from '../../services/shows.service';

@Component({
  selector: 'app-trailers',
  templateUrl: './trailers.component.html',
  styleUrls: ['./trailers.component.css']
})
export class TrailersComponent implements OnInit {
  isPageContentHidden: boolean = false;
  noData: boolean = false;
  typeofShow = 'trailer';
  trailerItem: any = [];
  TrailerTypes: any = [
    { name: 'Movie', value: 'movie' },
    { name: 'TV Show', value: 'tv_show' },
    { name: 'Documentary', value: 'documentary' },
    { name: 'Anime', value: 'anime' }
  ]
  constructor(private _showService: ShowsService) { }

  ngOnInit() {
    this._showService.getPublicShow(this.typeofShow, 'movie').subscribe(
      data => {
        this.trailerItem = data;
        console.log(this.trailerItem)
        if(this.trailerItem.length == 0){
          this.noData = true;
        }
        this.isPageContentHidden = true;
      },
      error => console.log(error)
    )
  }

  getTrailers(event){
    this._showService.getPublicShow(this.typeofShow, event.target.value).subscribe(
      data => {
        this.trailerItem = data;
        if(this.trailerItem.length == 0){
          this.noData = true;
        }
      },
      error => console.log(error)
    )
  }

}
