import { Component, OnInit } from '@angular/core';
import { ShowsService } from '../../services/shows.service';
import { UniqueValueArray } from '../../classes/UniqueValueArray';

@Component({
  selector: 'app-anime',
  templateUrl: './anime.component.html',
  styleUrls: ['./anime.component.css']
})
export class AnimeComponent implements OnInit {
  isPageContentHidden: boolean = false;
  noData: boolean = false;
  typeofShow = 'anime';
  tvshowArray: any = [];
  newTVArray: any = [];
  unique = new UniqueValueArray();
  constructor(private _showService: ShowsService) { }

  ngOnInit() {
    this._showService.getPublicShow(this.typeofShow, '').subscribe(
      (data) => {
        this.tvshowArray = data;
        if(this.tvshowArray.length == 0){
          this.noData = true;
        }
        this.isPageContentHidden = true;
      },
      error => console.log(error)
    )
  }

}
