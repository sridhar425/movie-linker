import { Component, OnInit } from '@angular/core';
import { ShowsService } from '../../services/shows.service';

@Component({
  selector: 'app-songs',
  templateUrl: './songs.component.html',
  styleUrls: ['./songs.component.css']
})
export class SongsComponent implements OnInit {
  noData: boolean = false;
  typeofShow = 'songs';
  songItem: any = [];
  isPageContentHidden: boolean = false;
  constructor(private _showService: ShowsService) { }

  ngOnInit() {
    this._showService.getPublicShow(this.typeofShow, '').subscribe(
      data => {
        this.songItem = data;
        if(this.songItem.length == 0){
          this.noData = true;
        }
        this.isPageContentHidden = true;
      },
      error => console.log(error)
    )
  }

}
