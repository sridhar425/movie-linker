import { Component, OnInit } from '@angular/core';
import { UserData } from '../../classes/user-data';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user = new UserData();
  result: any = {};
  userEmailCheck: any = {};
  constructor(private _auth: AuthService) { }

  ngOnInit() {
  }

  isUserExist(value){
    this._auth.checkUser(value)
    .subscribe(
      (email) => {
        this.userEmailCheck = email;
      },
      (error) => console.log(error)
    )
  }

  register(event){
    this._auth.registerData(this.user)
      .subscribe(
        (data) => {
          if(data.status == 202){
            this.result = data;
            this.result.msg = 'has been registered successfully!';
            this.result.class = 'alert-success alert text-center';
            (document.querySelector("#registerForm") as HTMLFormElement).reset();
          } else if(data.status == 500){
            this.result = data;
            this.result.msg = 'is already someone taken. Try other name!';
            this.result.class = 'alert-danger alert text-center'
          }
        },
        error => console.log(error)
      )
  }
}
