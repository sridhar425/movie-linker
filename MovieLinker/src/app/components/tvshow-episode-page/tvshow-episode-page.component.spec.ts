import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TvshowEpisodePageComponent } from './tvshow-episode-page.component';

describe('TvshowEpisodePageComponent', () => {
  let component: TvshowEpisodePageComponent;
  let fixture: ComponentFixture<TvshowEpisodePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TvshowEpisodePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TvshowEpisodePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
