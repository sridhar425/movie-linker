import { Component, OnInit } from '@angular/core';
import { ShowsService } from '../../services/shows.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UniqueValueArray } from '../../classes/UniqueValueArray';
import { AuthService } from '../../services/auth.service';
import { LikeOrDislike } from '../../classes/LikeOrDislike';

@Component({
  selector: 'app-tvshow-episode-page',
  templateUrl: './tvshow-episode-page.component.html',
  styleUrls: ['./tvshow-episode-page.component.css']
})
export class TvshowEpisodePageComponent extends LikeOrDislike implements OnInit {
  isPageContentHidden: boolean = false;
  unique = new UniqueValueArray();
  seriesId: any = this.activatedRoute.snapshot.paramMap.get('string').split('-')[2];
  series: any = {
    season:this.activatedRoute.snapshot.paramMap.get('string').split('-')[0],
    episode:this.activatedRoute.snapshot.paramMap.get('string').split('-')[1]
  }
  tvShowData: any = [];
  genre: any;
  constructor(public _show: ShowsService, public _auth: AuthService, private activatedRoute: ActivatedRoute,
    public _route: Router
  ) {
    super(_show, _auth, _route)
  }

  ngOnInit() {
    this._show.getSingleShow(this.seriesId, this.series)
      .subscribe(
        data => {
          let unique = new UniqueValueArray().getUniqShows(data[0].linkTable,'genre').toString().split(',');
          this.genre = unique.filter(function(item, pos, self) {
            return (self.indexOf(item.trim()) == pos);
          })
          this.tvShowData = data;
          console.log(this.tvShowData)
          this.isPageContentHidden = true;
        },
        error => console.log(error)
      )
      this._auth.isLoginAuth().subscribe( data => {
        this.userData = data
      })
  }

}
