import { Component, OnInit } from '@angular/core';
import { ShowsService } from '../../services/shows.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
@Component({
  selector: 'app-movie-page',
  templateUrl: './movie-page.component.html',
  styleUrls: ['./movie-page.component.css']
})
export class MoviePageComponent implements OnInit {
  groupItems: any = [{name:'',imae_name:''}];
  currentShowId: Number;
  isPageContentHidden: boolean = false;
  constructor(private _show: ShowsService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.currentShowId = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
    this._show.getShowGroup(this.currentShowId)
      .subscribe(
        (data) => {
          console.log(data)
          this.groupItems = data;
          this.isPageContentHidden = true;
        },
        error => console.log(error)
      )
  }

}
