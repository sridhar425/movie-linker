import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimeEpisodeListComponent } from './anime-episode-list.component';

describe('AnimeEpisodeListComponent', () => {
  let component: AnimeEpisodeListComponent;
  let fixture: ComponentFixture<AnimeEpisodeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnimeEpisodeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimeEpisodeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
