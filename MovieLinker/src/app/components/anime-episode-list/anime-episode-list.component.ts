import { Component, OnInit } from '@angular/core';
import { ShowsService } from '../../services/shows.service';
import { ActivatedRoute } from '@angular/router';
import { UniqueValueArray } from '../../classes/UniqueValueArray';

@Component({
  selector: 'app-anime-episode-list',
  templateUrl: './anime-episode-list.component.html',
  styles: []
})
export class AnimeEpisodeListComponent implements OnInit {
  currentShowNumber: any = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
  pageGeneralText: any = [];
  season: any;
  seasonForUi: any = [];
  selectedSeason: string;
  EpisodeArray = [];
  isPageContentHidden: boolean = false;

  unique = new UniqueValueArray();
  constructor(private _show: ShowsService, private activatedRoute: ActivatedRoute) { }
  
  ngOnInit() {
    this._show.getSeries(this.currentShowNumber, 'partOrSeason', '')
      .subscribe(
        (data) => {
          let count = 0;
          let seasonArray = [];
          // take season into array
          this.season = this.unique.getUniqShows(data, 'partOrSeason');
          for (let index = 0; index < this.season.length; index++) {
            seasonArray.push({
              "value": this.season[index],
              "label": "Season " + this.season[index].split('se')[1]
            })
            count++
          }
          if(this.season.length == count){
            this.seasonForUi = seasonArray;
            this.pageGeneralText = data[0];
          }
          this.isPageContentHidden = true;
        },
        error => console.log(error)
      )
  }
  getSeriesEpisode(event){
    if(event.target.value != ''){
      this._show.getSeries(this.currentShowNumber, event.target.value, 'episode')
      .subscribe((data) => {
        this.selectedSeason = event.target.value.charAt(0).toUpperCase() + event.target.value.substring(1);
        this.EpisodeArray = this.unique.getUniqShows(data, 'episode');
      })
    } else {
      this.EpisodeArray.splice(0);
    }
  }

}
