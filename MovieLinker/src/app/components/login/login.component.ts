import { Component, OnInit } from '@angular/core';
import { UserData } from '../../classes/user-data';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user = new UserData();
  result: any = {};
  constructor(private _auth: AuthService, private _route: Router) { }

  ngOnInit() {}

  login(event){
    this._auth.userLogin(this.user)
      .subscribe(
        (login) => {
          if(login.status == 200){
            window.location.href = '/profile';
          } else if(login.status == 503){
            this.result = {
              class: 'bg-danger text-center py-2 rounded',
              msg: 'No Accout found on this user!'
            }
          }else {
            this.result = {
              class: 'alert-danger text-center py-2 rounded',
              msg: 'Email or password might be invalid!'
            }
          }
        },
        (error) => console.log(error)
      )
  }

}
