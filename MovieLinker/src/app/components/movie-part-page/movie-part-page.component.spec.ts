import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoviePartPageComponent } from './movie-part-page.component';

describe('MoviePartPageComponent', () => {
  let component: MoviePartPageComponent;
  let fixture: ComponentFixture<MoviePartPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoviePartPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviePartPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
