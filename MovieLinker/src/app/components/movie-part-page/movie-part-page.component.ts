import { Component, OnInit } from '@angular/core';
import { ShowsService } from '../../services/shows.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UniqueValueArray } from '../../classes/UniqueValueArray';
import { AuthService } from '../../services/auth.service';
import { LikeOrDislike } from '../../classes/LikeOrDislike';

@Component({
  selector: 'app-movie-part-page',
  templateUrl: './movie-part-page.component.html',
  styleUrls: ['./movie-part-page.component.css']
})
export class MoviePartPageComponent extends LikeOrDislike implements OnInit {
  currentMovieId: Number;
  movieData: any = [];
  genre: any;
  isPageContentHidden: boolean = false;
  constructor(
    public _show: ShowsService,
    public _auth: AuthService,
    public _route: Router,
    private activatedRoute: ActivatedRoute
  ) { 
      super(_show, _auth, _route);
  }


  ngOnInit() {
    this.currentMovieId = parseInt(this.activatedRoute.snapshot.paramMap.get('id'))
    this._show.getSingleShow(this.currentMovieId, '')
      .subscribe(
        data => {
          if (Object.keys(data).length !=0) {
            let unique = new UniqueValueArray().getUniqShows(data[0].linkTable, 'genre').toString().split(',');
            this.genre = unique.filter(function (item, pos, self) {
              return (self.indexOf(item.trim()) == pos);
            })
            this.movieData = data;
            console.log(this.movieData)
            this.isPageContentHidden = true;
          }
        },
        error => console.log(error)
      )
      this._auth.isLoginAuth().subscribe( data => {
        this.userData = data
      })
  }

}
