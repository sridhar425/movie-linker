import { Component, OnInit } from '@angular/core';
import { ShowType } from '../../classes/ShowType';
import { UploadData } from '../../classes/upload-data';
import { ShowsService } from '../../services/shows.service';
import { NgForm, FormsModule, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-upload-section',
  templateUrl: './upload-section.component.html',
  styleUrls: ['./upload-section.component.css']
})
export class UploadSectionComponent implements OnInit {
  showFields: any; //this variable for getting show type class data
  public typeOfShow = new ShowType(); //creating object to access showType's show classes
  upload = new UploadData('defaultOption','','','','','','','','','','',0,''); //upload object class properties
  selectedFile: File = null; //this variable for upload image
  finalResult: any = [{
    class: '',
    text: ''
  }];
  queryId: any;
  
  public showTypes = [
    { name: 'Select Type of Show', value: 'defaultOption' },
    { name: 'Movie', value: 'movie' },
    { name: 'TV Show', value: 'tv_show' },
    { name: 'Documentary', value: 'documentary' },
    { name: 'Anime', value: 'anime' },
    { name: 'Songs', value: 'songs' },
    { name: 'Trailer', value: 'trailer' }
  ]
  constructor(private _show: ShowsService, private http: HttpClient, private activatedParam: ActivatedRoute, private _route: Router) { }

  ngOnInit() {
    console.log('test');
    this.queryId = parseInt(this.activatedParam.snapshot.queryParamMap.get('id'));
    if(isNaN(this.queryId)){
      this.showFields = this.typeOfShow.show("defaultOption");
    } else {
      this._show.getRequestShowForResponse(this.queryId).subscribe(
        data => {
          this.upload = data[0];
          this.showFields = this.typeOfShow.show(this.upload.showType);
        }, error => console.log(error)
      )
    }
  }
  //Show type selection
  onTypeSelect(event) {
    this.finalResult = [{ class: '', text: '' }];
    this.showFields = this.typeOfShow.show(this.upload.showType);
    if(event.target.value == 'trailer'){
      this.upload.trailerType = 'movie';
    }
  }

  // form resetting
  resetForm(f: NgForm) {
    f.resetForm();
    this.selectedFile = null; //reset file data especially image file
    this.upload.showType = 'defaultOption';
    this.showFields = this.typeOfShow.show(this.upload.showType);
  }

  //select image data from files
  onSelectedFile(event) {
    this.selectedFile = <File>event.target.files[0];
  }
  trailertype(event){
    if(event.target.value == 'movie' || event.target.value == 'documentary'){
      this.upload.episode = '';
    }
  }
  // Getting upload show data
  UploadShow(event) {
    this.upload.links = this.upload.links.split("\n");
    const fd = new FormData();
    if (this.selectedFile != null) {
      fd.append('image', this.selectedFile,  Date.now() + "-" + this.selectedFile.name);
    }
    const upload_data = JSON.stringify(this.upload);
    fd.append('upload_data', upload_data);
    let uploadType; 
    if(isNaN(this.queryId)){
      uploadType = this._show.uploadShowData(fd)
    } else {
      uploadType = this._show.requestResponseUpload(fd)
    }
    uploadType.subscribe(
      (data) => {
        if (data.status == 202) {
          if(!isNaN(this.queryId)){
            this._route.navigateByUrl('/requests')
          }
          (document.querySelector("#showform") as HTMLFormElement).reset(); //reset form data
          this.selectedFile = null; //reset file data especially image file
          this.upload.showType = 'defaultOption';
          this.showFields = this.typeOfShow.show(this.upload.showType);
          this.finalResult = [{
            class: 'bg-success p-1 text-center rounded mb-1',
            text: 'Updated Successfully'
          }];
        }
        if (data.status == 500) {
          (document.querySelector("#showform") as HTMLFormElement).reset(); //reset form data
          this.selectedFile = null; //reset file data especially image file
          this.upload.showType = 'defaultOption';
          this.showFields = this.typeOfShow.show(this.upload.showType);
          this.finalResult = [{
            class: 'bg-danger p-1 text-center rounded mb-1',
            text: 'Sorry,... Something went wrong,... try later!'
          }];
        }
      },
      error => console.log(error)
    )
  }
}
