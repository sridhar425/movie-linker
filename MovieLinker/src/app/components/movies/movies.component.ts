import { Component, OnInit } from '@angular/core';
import { ShowsService } from '../../services/shows.service';
@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  noData: boolean = false;
  typeofShow = 'movie';
  movieItem: any = [];
  isPageContentHidden: boolean = false;
  constructor(private _showService: ShowsService) { }

  ngOnInit() {
    this._showService.getPublicShow(this.typeofShow, '').subscribe(
      data => {
        this.movieItem = data;
        this.isPageContentHidden = true;
        if(this.movieItem.length == 0){
          this.noData = true;
        }
      },
      error => console.log(error)
    )
  }
}
