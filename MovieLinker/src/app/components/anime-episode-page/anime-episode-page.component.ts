import { Component, OnInit } from '@angular/core';
import { ShowsService } from '../../services/shows.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UniqueValueArray } from '../../classes/UniqueValueArray';
import { LikeOrDislike } from '../../classes/LikeOrDislike';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-anime-episode-page',
  templateUrl: './anime-episode-page.component.html',
  styles: []
})
export class AnimeEpisodePageComponent extends LikeOrDislike implements OnInit {
  isPageContentHidden: boolean = false;
  unique = new UniqueValueArray();
  seriesId: any = this.activatedRoute.snapshot.paramMap.get('string').split('-')[2];
  series: any = {
    season:this.activatedRoute.snapshot.paramMap.get('string').split('-')[0],
    episode:this.activatedRoute.snapshot.paramMap.get('string').split('-')[1]
  }
  animeData: any = [];
  genre: any;
  constructor(
    public _show: ShowsService, 
    public _auth: AuthService,
    private activatedRoute: ActivatedRoute,
    public _route: Router
  ) {
    super(_show, _auth, _route)
   }

  ngOnInit() {
    this._show.getSingleShow(this.seriesId, this.series)
      .subscribe(
        data => {
          let unique = new UniqueValueArray().getUniqShows(data[0].linkTable,'genre').toString().split(',');
          this.genre = unique.filter(function(item, pos, self) {
            return (self.indexOf(item.trim()) == pos);
          })
          this.animeData = data;
          this.isPageContentHidden = true;
        },
        error => console.log(error)
      )
    this._auth.isLoginAuth().subscribe( data => {
      this.userData = data
    })
  }

}
