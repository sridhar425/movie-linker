import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimeEpisodePageComponent } from './anime-episode-page.component';

describe('AnimeEpisodePageComponent', () => {
  let component: AnimeEpisodePageComponent;
  let fixture: ComponentFixture<AnimeEpisodePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnimeEpisodePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimeEpisodePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
