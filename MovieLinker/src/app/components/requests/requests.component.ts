import { Component, OnInit } from '@angular/core';
import { ShowsService } from '../../services/shows.service';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styles: []
})
export class RequestsComponent implements OnInit {
  isPageContentHidden: boolean = false;
  requestObj: any = [];
  constructor(private _show:ShowsService) { }

  ngOnInit() {
    this._show.requestedShow().subscribe(
      data => {
        console.log(data)
        this.requestObj = data;
        this.isPageContentHidden = true;
      },
      error => console.log(error)
      
    )
  }

}
