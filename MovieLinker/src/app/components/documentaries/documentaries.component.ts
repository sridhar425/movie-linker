import { Component, OnInit } from '@angular/core';
import { ShowsService } from '../../services/shows.service';

@Component({
  selector: 'app-documentaries',
  templateUrl: './documentaries.component.html',
  styleUrls: ['./documentaries.component.css']
})
export class DocumentariesComponent implements OnInit {
  noData: boolean = false;
  parameterType: string = 'documentary';
  docArray: any = [];
  isPageContentHidden: boolean = false;
  constructor(private _show: ShowsService) {}

  ngOnInit() {
    this._show.getPublicShow(this.parameterType, '')
      .subscribe(
        data => {
          this.docArray = data;
          if(this.docArray.length == 0){
            this.noData = true;
          }
          this.isPageContentHidden = true;
        },
        error => console.log(error)
      )
    
  }

}
