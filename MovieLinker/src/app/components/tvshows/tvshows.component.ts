import { Component, OnInit } from '@angular/core';
import { ShowsService } from '../../services/shows.service';
import { UniqueValueArray } from '../../classes/UniqueValueArray';

@Component({
  selector: 'app-tvshows',
  templateUrl: './tvshows.component.html',
  styleUrls: ['./tvshows.component.css']
})
export class TvshowsComponent implements OnInit {
  isPageContentHidden: boolean = false;
  noData: boolean = false;
  typeofShow = 'tv_show';
  tvshowArray: any = [];
  newTVArray: any = [];
  unique = new UniqueValueArray();
  constructor(private _showService: ShowsService) { }

  ngOnInit() {
    this._showService.getPublicShow(this.typeofShow, '').subscribe(
      (data) => {
        this.tvshowArray = data;
        if(this.tvshowArray.length == 0){
          this.noData = true;
        }
        this.isPageContentHidden = true;
      },
      error => console.log(error)
    )
  }
}
