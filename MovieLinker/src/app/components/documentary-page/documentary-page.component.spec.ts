import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentaryPageComponent } from './documentary-page.component';

describe('DocumentaryPageComponent', () => {
  let component: DocumentaryPageComponent;
  let fixture: ComponentFixture<DocumentaryPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentaryPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentaryPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
