import { Component, OnInit } from '@angular/core';
import { ShowsService } from '../../services/shows.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UniqueValueArray } from '../../classes/UniqueValueArray';
import { LikeOrDislike } from '../../classes/LikeOrDislike';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-documentary-page',
  templateUrl: './documentary-page.component.html',
  styleUrls: ['./documentary-page.component.css']
})
export class DocumentaryPageComponent extends LikeOrDislike implements OnInit {
  docId: Number;
  genre: any;
  docData: any = [];
  isPageContentHidden: boolean = false;
  constructor(public _show: ShowsService, private activatedRoute: ActivatedRoute, public _auth: AuthService,
    public _route: Router
  ) {
    super(_show, _auth, _route)
   }

  ngOnInit() {
    this.docId = parseInt(this.activatedRoute.snapshot.paramMap.get('id'))
    this._show.getSingleShow(this.docId, '')
      .subscribe(
        data => {
          let unique = new UniqueValueArray().getUniqShows(data[0].linkTable,'genre').toString().split(',');
          this.genre = unique.filter(function(item, pos, self) {
            return (self.indexOf(item.trim()) == pos);
          })
          this.docData = data;
          console.log(this.docData)
          this.isPageContentHidden = true;
        },
        error => console.log(error)
      )
      this._auth.isLoginAuth().subscribe( data => {
        this.userData = data
      })
  }

}
