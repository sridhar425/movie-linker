import { Component, OnInit } from '@angular/core';
import { ShowType } from '../../classes/ShowType';
import { UploadData } from '../../classes/upload-data';
import { ShowsService } from '../../services/shows.service';
import { NgForm, FormsModule, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-request-link',
  templateUrl: './request-link.component.html',
  styles: []
})
export class RequestLinkComponent implements OnInit {
  showFields: any; //this variable for getting show type class data
  public typeOfShow = new ShowType(); //creating object to access showType's show classes
  Request = new UploadData('defaultOption','','','','','','','','','','',0,''); //upload object class properties
  selectedFile: File = null; //this variable for upload image
  finalResult: any = [{
    class: '',
    text: ''
  }];
  public showTypes = [
    { name: 'Select Type of Show', value: 'defaultOption' },
    { name: 'Movie', value: 'movie' },
    { name: 'TV Show', value: 'tv_show' },
    { name: 'Documentary', value: 'documentary' },
    { name: 'Anime', value: 'anime' },
    { name: 'Songs', value: 'songs' },
    { name: 'Trailer', value: 'trailer' }
  ]
  constructor(private _show: ShowsService, private http: HttpClient) { }

  ngOnInit() {
    this.showFields = this.typeOfShow.show("defaultOption");
  }
  //Show type selection
  onTypeSelect(event) {
    this.finalResult = [{ class: '', text: '' }];
    this.showFields = this.typeOfShow.show(this.Request.showType);
    if(event.target.value == 'trailer'){
      this.Request.trailerType = 'movie';
    }
  }
  trailertype(event){
    if(event.target.value == 'movie' || event.target.value == 'documentary'){
      this.Request.episode = '';
    }
  }

  // form resetting
  resetForm(f: NgForm) {
    f.resetForm();
    this.selectedFile = null; //reset file data especially image file
    this.Request.showType = 'defaultOption';
    this.showFields = this.typeOfShow.show(this.Request.showType);
  }


  // Getting upload show data
  RequestShow(event) {
    this._show.requestShow(this.Request)
      .subscribe(
        (data) => {
          if (data.status == 202) {
            (document.querySelector("#showform") as HTMLFormElement).reset(); //reset form data
            this.selectedFile = null; //reset file data especially image file
            this.Request.showType = 'defaultOption';
            this.showFields = this.typeOfShow.show(this.Request.showType);
            this.finalResult = [{
              class: 'bg-success p-1 text-center rounded mb-1',
              text: 'Your request has been placed Successfully!'
            }];
          }
          if (data.status == 500) {
            (document.querySelector("#showform") as HTMLFormElement).reset(); //reset form data
            this.selectedFile = null; //reset file data especially image file
            this.Request.showType = 'defaultOption';
            this.showFields = this.typeOfShow.show(this.Request.showType);
            this.finalResult = [{
              class: 'bg-danger p-1 text-center rounded mb-1',
              text: 'Sorry,... Something went wrong,... try later!'
            }];
          }
        },
        error => console.log(error)
      )
  }
}
