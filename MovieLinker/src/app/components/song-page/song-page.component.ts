import { Component, OnInit } from '@angular/core';
import { ShowsService } from '../../services/shows.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UniqueValueArray } from '../../classes/UniqueValueArray';
import { LikeOrDislike } from '../../classes/LikeOrDislike';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-song-page',
  templateUrl: './song-page.component.html',
  styles: []
})
export class SongPageComponent extends LikeOrDislike implements OnInit {
  currentSongId: Number;
  songData: any = [];
  genre: any;
  isPageContentHidden: boolean = false;
  
  constructor(public _show: ShowsService, public _auth: AuthService, private activatedRoute: ActivatedRoute,
    public _route: Router
  ) {
    super(_show, _auth, _route)
   }

  ngOnInit() {
    this.currentSongId = parseInt(this.activatedRoute.snapshot.paramMap.get('id'))
    this._show.getSingleShow(this.currentSongId,'')
      .subscribe(
        data => {
          let unique = new UniqueValueArray().getUniqShows(data[0].linkTable,'genre').toString().split(',');
          this.genre = unique.filter(function(item, pos, self) {
            return (self.indexOf(item.trim()) == pos);
          })
          this.songData = data;
          this.isPageContentHidden = true;
        },
        error => console.log(error)
      )
      this._auth.isLoginAuth().subscribe( data => {
        this.userData = data
      })
  }
}
