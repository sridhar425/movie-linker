import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UploadData } from '../classes/upload-data';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ShowsService {
  constructor(private _http: HttpClient) { }
  // upload show data into db
  uploadShowData(fd: any): Observable<any>{
    return this._http.post<any>('/api/upload-show', fd);
  }
  //adding request into db
  requestShow(show: any){
    return this._http.post<any>('/api/request-show', show);
  }

  //getting requested shows fron db
  requestedShow(){
    return this._http.get<any>('/api/requests');
  }

  //getting request show for response
  getRequestShowForResponse(requestShowId){
    return this._http.post('/api/request-show-details', {requestShowId})
  }

  requestResponseUpload(fd: any): Observable<any>{
    return this._http.post<any>('/api/request-response', fd);
  }

  showVote(voteProp): Observable<any>{
    return this._http.post<any>('/api/vote', voteProp)
  }

  //getting show data for public page like movie, tv show, etc,..
  getPublicShow(currentShowType, forTrailer) {
    return this._http.post<UploadData>('/api/show-for-public', {currentShowType, forTrailer});
  }

  //getting group of shows for movie, etc...
  getShowGroup(currentShowId) {
    return this._http.post<UploadData>('/api/show-group', {currentShowId});
  }

  //getting final single show values on final page
  getSingleShow(currentFinalShowId, series:any){
    return this._http.post('/api/single-show', {currentFinalShowId, series});
  }
  //getting group of shows for tv series, Anime
  getSeries(currentShowNumber, season, episode) {
    return this._http.post<UploadData>('/api/show-series', {currentShowNumber,season,episode});
  }
}
