import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  constructor(private _http: HttpClient) { }
  // user register
  registerData(user): Observable<any>{
    return this._http.post('/api/register', user);
  }
// username checking from database before register
  checkUser(email){
    let nameCheck = true;
    return this._http.post('/api/register', {email, nameCheck})
  }

  // login user
  userLogin(loginUser): Observable<any>{
    return this._http.post('/api/login', loginUser)
  }

  // check login
  isLoginAuth(): Observable<any>{
    return this._http.get('/api/data');
  }
  //logout request
  logout(): Observable<any>{
    return this._http.get('/api/logout')
  }
}
