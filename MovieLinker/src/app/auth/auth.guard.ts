import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  
 constructor(private _auth: AuthService, private router: Router){}
 canActivate(
  next: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
 ): Observable<boolean> | Promise<boolean> | boolean{
  return this._auth.isLoginAuth().pipe(map(
    (data) => {
      if(data.status == true){
        return true;
      } else {
        this.router.navigate(['login'])
        return false;
      }
    }
  ))
 }
}
