module.exports = {
    load: () => {
        express = require('express');
        path = require('path');
        bodyParser = require('body-parser'); //middleware
        multer = require('multer'); //middleware for handling form data
        bcrypt = require('bcrypt'); //For encrypt and decrypt the password

        session = require('express-session') //session
        redis = require('redis');
        RedisStore = require('connect-redis')(session);
        redisClient = redis.createClient('redis://xyz.com:GNS1kBAmBxOwWLXR1XccjeEE8DErDIuE@redis-10339.c61.us-east-1-3.ec2.cloud.redislabs.com:10339');

        Sequelize = require('sequelize');
        Op = Sequelize.Op
        sequelize = require('../seqModules/db') //connect to db


        User = require('../seqModules/Users');
        Shows = require('../seqModules/Shows');
        Links = require('../seqModules/Links');
        Vote = require('../seqModules/Vote');
        Review = require('../seqModules/Review');


        linkAssociation = Shows.hasMany(Links, { as: 'linkTable', foreignKey: 'id_for_show' });
        votes = Links.hasMany(Vote, { as: 'voteTable', foreignKey: 'link_id' })
        count = Links.hasMany(Vote, { as: 'voteCount', foreignKey: 'link_id' })

        app = express();
        app.use(express.static(path.join(__dirname, './declaration.js')))
        app.use(bodyParser.json());
        app.use("/static", express.static(path.join(__dirname, '../uploads')))

        // session declare and initialization
        app.use(session({
            secret: 'sdffSKFDJFKLJJL15451ddfsgsSDFGS45',
            resave: false,
            saveUninitialized: true,
            store: new RedisStore({ client: redisClient })
        }))
        app.use(function (req, res, next) {
            if (!req.session) {
                return next(new Error('oh no')) // handle error
            }
            next() // otherwise continue
        })
    }
}