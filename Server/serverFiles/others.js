module.exports = {
    // checking any null values from object from client before to save it into db. if it has then it excluded from object
    deleteEmptyValue: (item) => {
        var sizeOfObj = Object.keys(item).length;
        var index = 0;
        for (var key in item) {
            if (item[key] == '' || item[key] == null) {
                delete item[key];
            }
            index++;
        }
        if (index == sizeOfObj) {
            return item;
        }
    },
    getUniqShows: (value, type) => {
        var eleArray = value;
        var eleArrayLowerCase = [];
        var count = 0;
        var dataName = eleArray.map(function (item) {
            return item[type];
        })
        dataName.forEach(key => {
            eleArrayLowerCase.push(key.toLowerCase());
            count++;
        })
        if (count == dataName.length) {
            eleArrayLowerCase.toString()
            return eleArrayLowerCase.filter(function (item, pos) {
                return eleArrayLowerCase.indexOf(item) == pos;
            })
        }
    }
}