module.exports = {
    show: () => {
        //multer image storage
        var storage = multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, './uploads')
            },
            filename: function (req, file, cb) {
                cb(null, file.originalname)
            }
        })
        var upload = multer({
            storage: storage
        })

        // 1. --> Upload Show (--> DB)
        // 2. --> Create Request Link (--> DB)
        // 3. --> Show Requests to page (<-- DB)
        // 4. --> get request detail to upload page (<-- DB)
        // 5. --> create request links into db (--> DB)
        // 6. --> All shows to Client (<-- DB)
        // 7. --> shows group (movies, tv shows, anime Ex. part1, part2, etc...)
        // 8. --> Getting Links to individual shows (<-- DB)
        // 8. --> Getting series season and episodes (<-- DB)

        //1. Upload show into database
        app.post('/api/upload-show', upload.single('image'), (req, res, next) => {
            let uploadShowData = JSON.parse(req.body.upload_data);
            delete uploadShowData.image;
            if (req.file != undefined) {
                uploadShowData.image_name = req.file.originalname; //for file name(here it is image)
            }
            uploadShowData = Others.deleteEmptyValue(uploadShowData); //removing null properties
            if (req.session.user) {
                let optionToCheckRow = {
                    showType: uploadShowData.showType,
                    name: uploadShowData.name
                }
                switch (optionToCheckRow.showType) {
                    case 'movie':
                        optionToCheckRow.partOrSeason = uploadShowData.partOrSeason
                        break;
                    case 'documentary':
                        optionToCheckRow.partOrSeason = uploadShowData.partOrSeason
                        break;
                    case 'tv_show':
                        optionToCheckRow.partOrSeason = uploadShowData.partOrSeason
                        optionToCheckRow.episode = uploadShowData.episode;
                        break;
                    case 'anime':
                        optionToCheckRow.partOrSeason = uploadShowData.partOrSeason;
                        optionToCheckRow.episode = uploadShowData.episode;
                        break;
                    case 'trailer':
                        switch (optionToCheckRow.trailerType) {
                            case 'movie':
                                optionToCheckRow.partOrSeason = uploadShowData.partOrSeason
                                break;
                            case 'documentary':
                                optionToCheckRow.partOrSeason = uploadShowData.partOrSeason
                                break;
                            case 'tv_show':
                                optionToCheckRow.partOrSeason = uploadShowData.partOrSeason
                                optionToCheckRow.episode = uploadShowData.episode;
                                break;
                            case 'anime':
                                optionToCheckRow.partOrSeason = uploadShowData.partOrSeason;
                                optionToCheckRow.episode = uploadShowData.episode;
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
                uploadShowData.trailerType != undefined ? optionToCheckRow.trailerType = uploadShowData.trailerType : optionToCheckRow;
                uploadShowData.episode != undefined ? optionToCheckRow.episode = uploadShowData.episode : optionToCheckRow;
                //Checking show detail already existed or not!
                Shows.findAll({
                    where: optionToCheckRow
                }).then(
                    row => {
                        //global into this block
                        let optionForLinksCreation = {
                            user: req.session.user,
                            created_by: req.session.user
                        }
                        //show existed Yes! push links only into database
                        if (row.length != 0) {
                            return sequelize.transaction(t => {
                                    var promises = [];
                                    optionForLinksCreation.image_name = uploadShowData.image_name;
                                    optionForLinksCreation.lannguage = uploadShowData.lannguage;
                                    optionForLinksCreation.genre = uploadShowData.genre;
                                    optionForLinksCreation.id_for_show = row[0].show_id;
                                    //adding links one by one
                                    for (let i = 0; i < uploadShowData.links.length; i++) {
                                        optionForLinksCreation.links = uploadShowData.links[i];
                                        promises.push(Links.create(optionForLinksCreation, {
                                            transaction: t
                                        }))
                                    }
                                    return Promise.all(promises);
                                }).then(data => {
                                    if (data.length == uploadShowData.links.length) {
                                        res.send(JSON.stringify({
                                            status: 202
                                        }))
                                    } else {
                                        res.send(JSON.stringify({
                                            status: 500
                                        }))
                                    }
                                })
                                .catch(
                                    error => console.log(error)
                                )
                        } else { //show not existed! so push every details into database
                            let optionForShowCreation = Object.assign(
                                uploadShowData, {
                                    created_by: req.session.user
                                }
                            );
                            return sequelize.transaction(t => {
                                    //creating show wit the details
                                    return Shows.create(optionForShowCreation, {
                                            transaction: t
                                        })
                                        .then(show => {
                                            var promises = [];
                                            optionForLinksCreation.image_name = optionForShowCreation.image_name;
                                            optionForLinksCreation.lannguage = uploadShowData.lannguage;
                                            optionForLinksCreation.genre = uploadShowData.genre;
                                            optionForLinksCreation.id_for_show = show.show_id;
                                            //adding links one by one
                                            for (let i = 0; i < optionForShowCreation.links.length; i++) {
                                                optionForLinksCreation.links = optionForShowCreation.links[i];
                                                promises.push(Links.create(optionForLinksCreation, {
                                                    transaction: t
                                                }))
                                            }
                                            return Promise.all(promises);
                                        });
                                }).then(data => {
                                    if (data.length == optionForShowCreation.links.length) {
                                        res.send(JSON.stringify({
                                            status: 202
                                        }))
                                    } else {
                                        res.send(JSON.stringify({
                                            status: 500
                                        }))
                                    }
                                })
                                .catch(
                                    error => console.log(error)
                                )
                        }
                    }
                )

            }
        })

        //2. Request links upload into db
        app.post('/api/request-show', (req, res, next) => {
            if (req.session.user) {
                let requestObj = req.body
                requestObj.requested_by = req.session.user;
                requestObj = Others.deleteEmptyValue(requestObj);
                //Adding request show query into database
                Shows.create(requestObj).then(result => {
                    if (result.name) {
                        res.send(JSON.stringify({
                            status: 202
                        }))
                    } else {
                        res.send(JSON.stringify({
                            status: 500
                        }))
                    }
                })
            }
        })

        //3. requested link getting from db with answer
        app.get('/api/requests', (req, res, next) => {
            Shows.findAll({ //Providing all request links to page
                where: {
                    requested_by: {
                        [Op.ne]: ''
                    }
                },
                include: [{
                    association: linkAssociation,
                    as: 'linkTable'
                }]
            }).then(
                result => {
                    if (result.length != 0) {
                        res.send(result)
                    } else {
                        res.send(JSON.stringify({
                            status: 404
                        }))
                    }
                }
            )
        })

        //4. getting request show details for upload page to fill fields.
        app.post('/api/request-show-details', (req, res, next) => {
            Shows.findAll({ //request details
                where: {
                    show_id: req.body.requestShowId
                }
            }).then(
                result => {
                    if (result.length != 0) {
                        res.send(result)
                    } else {
                        res.send(JSON.stringify({
                            status: 404
                        }))
                    }
                }
            )
        })

        //5. uploading links and images for requested show
        app.post('/api/request-response', upload.single('image'), (req, res, next) => {
            let linkUpload = JSON.parse(req.body.upload_data);
            if (req.file != undefined) {
                linkUpload.image_name = req.file.originalname; //for file name(here it is image)
            }
            delete linkUpload.image;
            if (req.session.user) {
                return sequelize.transaction(t => {
                    // update if first answer hasn't been updated.(create_by != '' | user)
                    return Shows.update({
                        created_by: req.session.user,
                        updatedAt: Date.now()
                    }, {
                        where: {
                            [Op.and]: {
                                show_id: linkUpload.show_id,
                                created_by: {
                                    [Op.eq]: ''
                                }
                            }
                        }
                    }, {
                        transaction: t
                    }).then(links => {
                        //uploading links which from request response page into database
                        var promises = [];
                        let linkFromRequestRespose = {
                            id_for_show: linkUpload.show_id,
                            user: req.session.user,
                            image_name: linkUpload.image_name,
                            links: linkUpload.links[i],
                            created_by: req.session.user
                        }
                        for (let i = 0; i < linkUpload.links.length; i++) {
                            promises.push(Links.create(linkFromRequestRespose, {
                                transaction: t
                            }))
                        }
                        return Promise.all(promises);
                    }).then(data => {
                        if (data.length == linkUpload.links.length) {
                            res.send(JSON.stringify({
                                status: 202
                            }))
                        } else {
                            res.send(JSON.stringify({
                                status: 500
                            }))
                        }
                    }).catch(
                        error => console.log(error)
                    )
                })
            }
        })

        //6. getting data for every show to place on public area of client
        app.post('/api/show-for-public', (req, res, next) => {
            var statement = {
                attributes: [
                    'show_id',
                    'name'
                ],
                where: {
                    showType: req.body.currentShowType
                },
                include: [{
                    association: linkAssociation,
                    as: 'linkTable',
                    attributes: ['image_name'],
                    where: {
                        id: {
                            [Op.ne]: null
                        }
                    }
                }]
            }
            switch (req.body.currentShowType) {
                case 'tv_show':
                    statement.group = ['name']
                    break;
                case 'anime':
                    statement.group = ['name']
                    break;
                case 'songs':
                    statement.attributes.push('singer')
                    break;
                case 'trailer':
                    statement.where.trailerType = req.body.forTrailer
                    break;
                default:
                    break;
            }
            Shows.findAll(statement).then((data) => {
                if (data.length > 0) {
                    data.status = 202;
                    res.send(JSON.stringify(data))
                } else {
                    data.status = 500;
                    res.send(JSON.stringify(data))
                }
                // res.send(JSON.stringify(data))
            }).catch(error => {
                console.log(error)
            })
        })
        //7. getting group of show on ther respective part or season
        app.post('/api/show-group', (req, res, next) => {
            return sequelize.transaction(t => {
                return Shows.findAll({ //get row with this show id
                    where: {
                        show_id: req.body.currentShowId
                    }
                }, {
                    transaction: t
                }).then(show => {
                    return Shows.findAll({ // get all row with name and show type
                        where: {
                            [Op.and]: {
                                name: show[0].name,
                                showType: show[0].showType
                            }
                        },
                        include: [{
                            association: linkAssociation,
                            as: 'linkTable',
                            attributes: ['image_name']
                        }]
                    }, {
                        transaction: t
                    })
                })
            }).then(
                data => {
                    if (data.length != 0) {
                        res.send(JSON.stringify(data))
                    } else {
                        res.send(JSON.stringify({
                            status: 500
                        }))
                    }
                }
            ).catch(error => {
                console.log(error)
            })
        })

        //8. getting final single show data which may be movie, documentary or others
        app.post('/api/single-show', (req, res, next) => {
            console.log(req.body)
            return sequelize.transaction(t => {
                return Shows.findAll({ //find row with show id
                    where: {
                        show_id: req.body.currentFinalShowId
                    }
                }, {
                    transaction: t
                }).then(show => {
                    let condStmt = {
                        name: show[0].name,
                        showType: show[0].showType,
                    }
                    if (req.body.series != '') { //if it is tvshows, anine.
                        condStmt.partOrSeason = req.body.series.season.toLowerCase(),
                            condStmt.episode = req.body.series.episode.toLowerCase()
                    } else {
                        condStmt.partOrSeason = show[0].partOrSeason
                    }
                    return Shows.findAll({ // get rows with links
                        where: {
                            [Op.and]: condStmt
                        },
                        include: [{
                            association: linkAssociation,
                            as: 'linkTable',
                            attributes: ['id', 'id_for_show', 'genre', 'language', 'image_name', 'links', 'user'],
                            include: [
                                {
                                    association: votes,
                                    as: 'voteTable',
                                    attributes: ['link_id', 'vote', 'created_by']
                                }
                            ]
                            
                        }]
                    }, {
                        transaction: t
                    })
                })
            }).then((result) => {
                if (result.length != 0) {
                    res.send(JSON.stringify(result));
                } else {
                    res.send(JSON.stringify({
                        status: 500
                    }));
                }
            }).catch(error => {
                console.log(error)
            })
        })
        //9. getting series based on episode
        app.post('/api/show-series', (req, res, next) => {
            return sequelize.transaction(t => {
                console.log(req.body)
                    return Shows.findAll({ //get row with id
                        where: {
                            show_id: req.body.currentShowNumber
                        }
                    }, {
                        transaction: t
                    }).then(show => {
                        let whereStmt = {
                            name: show[0].name,
                            showType: show[0].showType
                        }
                        if (req.body.episode != '') { // its for to asign season while getting episode ex. se1, se2
                            whereStmt.partOrSeason = req.body.season
                        }
                        return Shows.findAll({
                            where: {
                                [Op.and]: whereStmt
                            },
                            group: [req.body.episode == '' ? req.body.season : req.body.episode], //group by season or episode
                            include: [{
                                association: linkAssociation,
                                as: 'linkTable',
                                attributes: ['image_name'],
                                where: {
                                    id: {
                                        [Op.ne]: null
                                    }
                                }
                            }]
                        }, {
                            transaction: t
                        })
                    })
                })
                .then((data) => {
                    if (data.length != 0) {
                        res.send(data)
                    } else {
                        res.send(JSON.stringify({
                            status: 404
                        }))
                    }
                }).catch(error => {
                    console.log(error)
                })
        })
    }
}