var configure = require("./configure.js").load();
Others = require('./others.js');
show = require('./show').show();
vote = require('./votes').votes();
review = require('./review').reviews()



//user registration
app.post('/api/register', (req, res, next) => {
  if (req.body.nameCheck) { //username checking if it is already existed or not
    User.findAll({
      attributes: ['email'],
      where: {
        email: req.body.email
      }
    })
      .then((result) => {
        if (result.length > 0) {
          res.send(JSON.stringify({ msg: 'Email is already existed! Make different email' }))
        } else {
          res.send(JSON.stringify({ msg: '' }))
        }
      })
      .catch(
        error => console.log(error)
      )
  } else { //registering user
    User.create(req.body)
      .then(data => {
        if (data.email) {
          res.send(JSON.stringify({
            name: data.email,
            status: 202
          }))
        }
      })
      .catch(
        error => {
          if (error.errors[0].type === 'unique violation') {
            console.log(error.errors[0].value)
            res.send(JSON.stringify({
              name: error.errors[0].value,
              status: 500
            }))
          }
        })
  }
})
// login data
app.post('/api/login', (req, res, next) => {
  User.findAll({
    where: {
      email: req.body.email
    }
  }).then((data) => {
    if (data.length == 0) {
      res.send(JSON.stringify({ status: 503 }));
    } else if (bcrypt.compareSync(req.body.password, data[0].password)) {
      req.session.user = req.body.email;
      req.session.save((err) => {
        if (err) throw err;
      });
      console.log(req.session)
      res.send(JSON.stringify({ status: 200 }));
    } else {
      res.send(JSON.stringify({ status: 404 }));
    }
  })
})

// session request for auth guard confirmation
app.get('/api/data', (req, res, next) => {
  if (req.session.user != undefined) {
    User.findAll({
      where: {
        userName: req.session.user
      }
    }).then((data) => {
      res.send(JSON.stringify({ status: true, user: req.session.user }))
    })
  } else {
    res.send(JSON.stringify({ status: false }))
  }
})

// logout function
app.get('/api/logout', (req, res, next) => {
  req.session.destroy((err) => {
    if (err) throw err;
    res.send(JSON.stringify({ status: true }));
  })
})



// sever port
app.listen(1200, (err) => {
  if (err) throw err;
  console.log('server running at 1200!')
});