module.exports = {
    votes: () => {
        // likes and dislikes section
        app.post('/api/vote', (req, res, next) => {
            if (req.session.user) {
                return sequelize.transaction(t => {
                    return Vote.findAll({ //SELECT * FROM votes WHERE link_id=? & created_by=?
                        where: {
                            [Op.and]: {
                                link_id: req.body.link_id,
                                created_by: req.session.user
                            }
                        }
                    }, {
                        transaction: t
                    }).then(item => {
                        if (item.length != 0) {
                            if (item[0].vote == req.body.vote) {
                                return Vote.destroy({ //Delete the vote
                                    where: {
                                        [Op.and]: {
                                            link_id: req.body.link_id,
                                            created_by: req.session.user
                                        }
                                    }
                                }, {transaction: t}).then(() => 'deleted')
                            } else {
                                return Vote.update({ //Update the vote
                                    vote: req.body.vote,
                                    updatedAt: Date.now()
                                },{
                                    where: {
                                        [Op.and]: {
                                            link_id: req.body.link_id,
                                            created_by: req.session.user
                                        }
                                    }
                                }, {transaction: t}).then(() => 'updated')
                            }
                            
                        } else {
                            return Vote.create(Object.assign(req.body, {
                                created_by: req.session.user
                            }), {
                                transaction: t
                            })
                        }
                    })
                }).then(result => {
                    if (result == 'deleted') { // If deleted the vote
                        res.send(JSON.stringify({
                            response: 'deleted',
                            vote: 0,
                            id: req.body.link_id
                        }))
                    } else if (result == 'updated') { // If updated the vote
                        res.send(JSON.stringify({
                            response: 'updated',
                            vote: req.body.vote,
                            id: req.body.link_id
                        }))
                    } else { // If created the vote
                        if (result.length != 0) {
                            res.send(JSON.stringify({
                                response: 'creadted',
                                vote: req.body.vote,
                                id: req.body.link_id
                            }))
                        } else {
                            res.send(JSON.stringify({
                                status: 500
                            }))
                        }
                    }
                })
            }
        })
    }
}