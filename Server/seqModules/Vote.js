const Sequilize = require('sequelize');
const db = require('./db');
const Vote = db.define('vote', {
    id: {
        type: Sequilize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    show_id: {
        type: Sequilize.INTEGER
    },
    link_id: {
        type: Sequilize.INTEGER
    },
    vote: {
        type: Sequilize.INTEGER
    },
    created_by: {
        type: Sequilize.STRING
    },
    createdAt: {
        type: Sequilize.DATE,
        defaultValue: Sequilize.NOW
    },
    updatedAt: {
        type: Sequilize.DATE,
        defaultValue: Sequilize.NOW
    }
},
    {
        timestamps: false
    });

module.exports = Vote;