const Sequilize = require('sequelize');
const db = require('./db');
const Review = db.define('reviews', {
    review_id: {
        type: Sequilize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    show_id: {
        type: Sequilize.INTEGER
    },
    link_id: {
        type: Sequilize.INTEGER
    },
    review_text: {
        type: Sequilize.BLOB
    },
    created_by: {
        type: Sequilize.STRING
    },
    createdAt: {
        type: Sequilize.DATE,
        defaultValue: Sequilize.NOW
    },
    updatedAt: {
        type: Sequilize.DATE,
        defaultValue: Sequilize.NOW
    }
},
    {
        timestamps: false
    });

module.exports = Review;