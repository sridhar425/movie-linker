const Sequilize = require('sequelize');
const db = require('./db');
const bcrypt = require('bcrypt');
const User = db.define('users', {
    id: {
        type: Sequilize.INTEGER,
        autoIncrement: false,
        primaryKey: true
    },
    userName: {
        type: Sequilize.STRING

    },
    email: {
        type: Sequilize.STRING,
        uniqueKey: true
    },
    password: {
        type: Sequilize.STRING
    },
    createdAt: {
        type: Sequilize.DATE,
        defaultValue: Sequilize.NOW
    },
    updatedAt: {
        type: Sequilize.DATE,
        defaultValue: Sequilize.NOW
    }
},
    {
        timestamps: false
    });

User.beforeCreate((user, options) => {
    var salt = bcrypt.genSaltSync(10);
    return user.password =  bcrypt.hashSync(user.password, salt);
})


module.exports = User;