const express = require('express');
const app = express();

const Sequilize = require('sequelize');
const db = new Sequilize('movie_linker', 'dbuser', 'dbuser', {
  host: 'localhost',
  dialect: 'mariadb'
})

db
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });
// This is only for database connection checking
// app.listen(1500, (err) => {
//     if (err) throw err;
//     console.log('server running at 1500!')
//     console.log(db);
//   });
module.exports = db;