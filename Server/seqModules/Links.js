const Sequilize = require('sequelize');
const db = require('./db');
const Links = db.define('show_links', {
    id: {
        type: Sequilize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    id_for_show: {
        type: Sequilize.INTEGER
    },
    user: {
        type: Sequilize.STRING
    },
    language: {
        type: Sequilize.STRING
    },
    genre: {
        type: Sequilize.STRING
    },
    image_name: {
        type: Sequilize.STRING
    },
    links: {
        type: Sequilize.STRING
    },
    created_by: {
        type: Sequilize.STRING
    },
    createdAt: {
        type: Sequilize.DATE,
        defaultValue: Sequilize.NOW
    },
    updatedAt: {
        type: Sequilize.DATE,
        defaultValue: Sequilize.NOW
    }
},
    {
        timestamps: false
    })
module.exports = Links;