const Sequilize = require('sequelize');
const db = require('./db');
const Links = require('./Links')
const Shows = db.define('shows', {
    show_id: {
        type: Sequilize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    showType: {
        type: Sequilize.STRING,
    },
    trailerType: {
        type: Sequilize.STRING
    },
    name: {
        type: Sequilize.STRING
    },
    singer: {
        type: Sequilize.STRING
    },
    partOrSeason: {
        type: Sequilize.STRING
    },
    episode: {
        type: Sequilize.STRING
    },
    requested_by: {
        type: Sequilize.STRING
    },
    created_by: {
        type: Sequilize.STRING
    },
    createdAt: {
        type: Sequilize.DATE,
        defaultValue: Sequilize.NOW
    },
    updatedAt: {
        type: Sequilize.DATE,
        defaultValue: Sequilize.NOW
    }
},
    {
        timestamps: false
    });




module.exports = Shows;