-- MySQL dump 10.16  Distrib 10.1.37-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: movie_linker
-- ------------------------------------------------------
-- Server version	10.1.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `show_links`
--

DROP TABLE IF EXISTS `show_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `show_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_for_show` int(11) NOT NULL,
  `user` varchar(250) NOT NULL,
  `image_name` varchar(250) NOT NULL,
  `links` varchar(250) NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `show_links`
--

LOCK TABLES `show_links` WRITE;
/*!40000 ALTER TABLE `show_links` DISABLE KEYS */;
INSERT INTO `show_links` VALUES (1,1,'admin','room.jpg','https://yts.am/movie/room-2015','admin','2019-06-04 06:29:48','2019-06-04 06:29:48'),(2,1,'admin','room.jpg','https://yts.am/movie/room-2015-2','admin','2019-06-04 06:29:48','2019-06-04 06:29:48'),(3,1,'admin','room.jpg','https://yts.am/movie/room-2015-3','admin','2019-06-04 06:29:48','2019-06-04 06:29:48'),(4,2,'admin','1559632946335-captain marvel.jpg','https://yts.am/movie/captain-marvel-2019','admin','2019-06-04 07:22:32','2019-06-04 07:22:32'),(5,3,'admin','1559633196215-medium-cover.jpg','https://yts.am/movie/how-to-train-your-dragon-the-hidden-world-2019','admin','2019-06-04 07:26:36','2019-06-04 07:26:36'),(6,4,'admin','1559633291603-medium-cover (1).jpg','https://yts.am/movie/mortal-engines-2018','admin','2019-06-04 07:28:12','2019-06-04 07:28:12'),(7,5,'admin','1559633368464-terminator.jpg','https://yts.am/movie/the-terminator-1984','admin','2019-06-04 07:29:29','2019-06-04 07:29:29'),(8,6,'admin','1559633449388-terminator.jpg','https://yts.am/movie/terminator-2-judgment-day-1991','admin','2019-06-04 07:30:50','2019-06-04 07:30:50'),(9,7,'admin','1559633548611-terminator.jpg','https://yts.am/movie/terminator-3-rise-of-the-machines-2003','admin','2019-06-04 07:32:29','2019-06-04 07:32:29'),(10,8,'admin','1559633622579-terminator.jpg','https://yts.am/movie/terminator-genisys-2015','admin','2019-06-04 07:33:43','2019-06-04 07:33:43'),(11,9,'admin','1559633701525-harry-potter.jpg','https://yts.am/movie/harry-potter-and-the-sorcerers-stone-2001','admin','2019-06-04 07:35:02','2019-06-04 07:35:02'),(12,10,'admin','1559633793357-harry-potter.jpg','https://yts.am/movie/harry-potter-and-the-chamber-of-secrets-2002','admin','2019-06-04 07:36:34','2019-06-04 07:36:34'),(13,11,'admin','1559650759668-captain marvel.jpg','https://yts.am/movie/room-2015','admin','2019-06-04 12:19:20','2019-06-04 12:19:20'),(14,12,'admin','1559650803384-captain marvel.jpg','https://yts.am/movie/room-2015-4','admin','2019-06-04 12:20:04','2019-06-04 12:20:04'),(15,13,'admin','1559720847942-room.jpg','https://yts.am/movie/room-2015','admin','2019-06-05 07:47:30','2019-06-05 07:47:30'),(16,13,'admin','1559720847942-room.jpg','https://yts.am/movie/room-2015','admin','2019-06-05 07:47:30','2019-06-05 07:47:30'),(17,14,'admin','1559720924959-room.jpg','https://yts.am/movie/room-2015','admin','2019-06-05 07:48:45','2019-06-05 07:48:45'),(18,14,'admin','1559720924959-room.jpg','https://yts.am/movie/room-2015','admin','2019-06-05 07:48:45','2019-06-05 07:48:45'),(19,14,'admin','1559720924959-room.jpg','https://yts.am/movie/room-2015','admin','2019-06-05 07:48:45','2019-06-05 07:48:45'),(20,15,'admin','1559720990125-room.jpg','https://yts.am/movie/room-2015','admin','2019-06-05 07:49:50','2019-06-05 07:49:50'),(21,16,'admin','1559721160359-escaperoom.jpg','https://yts.am/movie/escape-room-2017','admin','2019-06-05 07:52:41','2019-06-05 07:52:41'),(22,17,'admin','1559721299411-medium-cover (2).jpg','https://yts.am/movie/the-room-2003','admin','2019-06-05 07:55:00','2019-06-05 07:55:00'),(23,18,'admin','1559722103754-room.jpg','https://yts.am/movie/room-2015','admin','2019-06-05 08:08:24','2019-06-05 08:08:24'),(24,19,'admin','1559722140195-room.jpg','https://yts.am/movie/room-2015','admin','2019-06-05 08:09:00','2019-06-05 08:09:00'),(25,19,'admin','1559722140195-room.jpg','https://yts.am/movie/room-2015','admin','2019-06-05 08:09:00','2019-06-05 08:09:00'),(26,20,'admin','1559731532655-terminator.jpg','https://yts.am/movie/the-terminator-1984','admin','2019-06-05 10:45:33','2019-06-05 10:45:33');
/*!40000 ALTER TABLE `show_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shows`
--

DROP TABLE IF EXISTS `shows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shows` (
  `show_id` int(11) NOT NULL AUTO_INCREMENT,
  `showType` varchar(250) NOT NULL,
  `trailerType` varchar(100) NOT NULL,
  `language` varchar(250) NOT NULL,
  `genre` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `singer` varchar(250) NOT NULL,
  `partOrSeason` varchar(250) NOT NULL,
  `episode` varchar(250) NOT NULL,
  `requested_by` varchar(250) NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`show_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shows`
--

LOCK TABLES `shows` WRITE;
/*!40000 ALTER TABLE `shows` DISABLE KEYS */;
INSERT INTO `shows` VALUES (1,'movie','','English','Drama, Thriller','Room','','2015','','','admin','2019-06-04 06:29:48','2019-06-04 06:29:48'),(2,'movie','','English','Action, Sci-fi','Captain Marvel','','2019','','','admin','2019-06-04 07:22:29','2019-06-04 07:22:29'),(3,'movie','','English','Action','How to Train You Dragon','','The hidden world','','','admin','2019-06-04 07:26:36','2019-06-04 07:26:36'),(4,'movie','','English','Action, Thriller, Fantasy','Mortal Engine','','2018','','','admin','2019-06-04 07:28:12','2019-06-04 07:28:12'),(5,'movie','','English','Action, Sci-fi','Terminator','','1984','','','admin','2019-06-04 07:29:29','2019-06-04 07:29:29'),(6,'movie','','English','Action, Sci-fi','Terminator','','Judgement Day','','','admin','2019-06-04 07:30:50','2019-06-04 07:30:50'),(7,'movie','','English','Action, Sci-fi','Terminator','','Rise of the machines','','','admin','2019-06-04 07:32:29','2019-06-04 07:32:29'),(8,'movie','','English','Action, Sci-fi','Terminator','','Genisys','','','admin','2019-06-04 07:33:43','2019-06-04 07:33:43'),(9,'movie','','English','Drama, Fantasy, Adventure','Harry Potter','','Sorcerer\'s Stone','','','admin','2019-06-04 07:35:02','2019-06-04 07:35:02'),(10,'movie','','English','Adventure, Fantasy','Harry Potter','','The Chamber of Secret','','','admin','2019-06-04 07:36:34','2019-06-04 07:36:34'),(11,'movie','','English','Action, Sci-fi','Captain Marvel','','2019','','','admin','2019-06-04 12:19:20','2019-06-04 12:19:20'),(12,'movie','','English','Action, Sci-fi','Captain Marvel','','2019','','','admin','2019-06-04 12:20:04','2019-06-04 12:20:04'),(13,'movie','','English','Thriller, Drama','Room Brie Larson','','2015','','','admin','2019-06-05 07:47:29','2019-06-05 07:47:29'),(14,'movie','','Enlish','Thriller, Drama','Movie Room','','2015','','','admin','2019-06-05 07:48:45','2019-06-05 07:48:45'),(15,'movie','','English','Drama, Thriller','movie room movie','','2015','','','admin','2019-06-05 07:49:50','2019-06-05 07:49:50'),(16,'movie','','Enlish','Horrer','Escape Room','','2017','','','admin','2019-06-05 07:52:41','2019-06-05 07:52:41'),(17,'movie','','English','Action, comedy, drama','The Room','','2003','','','admin','2019-06-05 07:55:00','2019-06-05 07:55:00'),(18,'movie','','English','Thriller','room','','2015','','','admin','2019-06-05 08:08:24','2019-06-05 08:08:24'),(19,'movie','','English','Drama','room','','2015','','','admin','2019-06-05 08:09:00','2019-06-05 08:09:00'),(20,'movie','','English','Action, Sci-fi','Terminator','','Judgement Day','','','admin','2019-06-05 10:45:33','2019-06-05 10:45:33');
/*!40000 ALTER TABLE `shows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(200) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`userName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin@gmail.com','$2b$10$jSNfZBe.755ps4H21RXIl.AoWN.mQV3u91JbHkNKzLOkOaaGKLLF2','2019-06-03 06:47:23','2019-06-03 06:47:23');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-05 17:12:35
