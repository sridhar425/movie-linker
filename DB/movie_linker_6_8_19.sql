-- MySQL dump 10.16  Distrib 10.1.37-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: movie_linker
-- ------------------------------------------------------
-- Server version	10.1.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `show_links`
--

DROP TABLE IF EXISTS `show_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `show_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_for_show` int(11) NOT NULL,
  `user` varchar(250) NOT NULL,
  `image_name` varchar(250) NOT NULL,
  `links` varchar(250) NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `show_links`
--

LOCK TABLES `show_links` WRITE;
/*!40000 ALTER TABLE `show_links` DISABLE KEYS */;
INSERT INTO `show_links` VALUES (1,1,'admin@gmail.com','room.jpg','https://yts.am/movie/room-2015','admin@gmail.com','2019-06-04 06:29:48','2019-06-04 06:29:48'),(2,1,'admin@gmail.com','room.jpg','https://yts.am/movie/room-2015-2','admin@gmail.com','2019-06-04 06:29:48','2019-06-04 06:29:48'),(3,1,'admin@gmail.com','room.jpg','https://yts.am/movie/room-2015-3','admin@gmail.com','2019-06-04 06:29:48','2019-06-04 06:29:48'),(4,2,'admin@gmail.com','1559632946335-captain marvel.jpg','https://yts.am/movie/captain-marvel-2019','admin@gmail.com','2019-06-04 07:22:32','2019-06-04 07:22:32'),(5,3,'admin@gmail.com','1559633196215-medium-cover.jpg','https://yts.am/movie/how-to-train-your-dragon-the-hidden-world-2019','admin@gmail.com','2019-06-04 07:26:36','2019-06-04 07:26:36'),(6,4,'admin@gmail.com','1559633291603-medium-cover (1).jpg','https://yts.am/movie/mortal-engines-2018','admin@gmail.com','2019-06-04 07:28:12','2019-06-04 07:28:12'),(7,5,'admin@gmail.com','1559633368464-terminator.jpg','https://yts.am/movie/the-terminator-1984','admin@gmail.com','2019-06-04 07:29:29','2019-06-04 07:29:29'),(8,6,'admin@gmail.com','1559633449388-terminator.jpg','https://yts.am/movie/terminator-2-judgment-day-1991','admin@gmail.com','2019-06-04 07:30:50','2019-06-04 07:30:50'),(9,7,'admin@gmail.com','1559633548611-terminator.jpg','https://yts.am/movie/terminator-3-rise-of-the-machines-2003','admin@gmail.com','2019-06-04 07:32:29','2019-06-04 07:32:29'),(10,8,'admin@gmail.com','1559633622579-terminator.jpg','https://yts.am/movie/terminator-genisys-2015','admin@gmail.com','2019-06-04 07:33:43','2019-06-04 07:33:43'),(11,9,'admin@gmail.com','1559633701525-harry-potter.jpg','https://yts.am/movie/harry-potter-and-the-sorcerers-stone-2001','admin@gmail.com','2019-06-04 07:35:02','2019-06-04 07:35:02'),(12,10,'admin@gmail.com','1559633793357-harry-potter.jpg','https://yts.am/movie/harry-potter-and-the-chamber-of-secrets-2002','admin@gmail.com','2019-06-04 07:36:34','2019-06-04 07:36:34'),(13,11,'admin@gmail.com','1559650759668-captain marvel.jpg','https://yts.am/movie/room-2015','admin@gmail.com','2019-06-04 12:19:20','2019-06-04 12:19:20'),(14,12,'admin@gmail.com','1559650803384-captain marvel.jpg','https://yts.am/movie/room-2015-4','admin@gmail.com','2019-06-04 12:20:04','2019-06-04 12:20:04'),(15,13,'admin@gmail.com','1559720847942-room.jpg','https://yts.am/movie/room-2015','admin@gmail.com','2019-06-05 07:47:30','2019-06-05 07:47:30'),(16,13,'admin@gmail.com','1559720847942-room.jpg','https://yts.am/movie/room-2015','admin@gmail.com','2019-06-05 07:47:30','2019-06-05 07:47:30'),(17,14,'admin@gmail.com','1559720924959-room.jpg','https://yts.am/movie/room-2015','admin@gmail.com','2019-06-05 07:48:45','2019-06-05 07:48:45'),(18,14,'admin@gmail.com','1559720924959-room.jpg','https://yts.am/movie/room-2015','admin@gmail.com','2019-06-05 07:48:45','2019-06-05 07:48:45'),(19,14,'admin@gmail.com','1559720924959-room.jpg','https://yts.am/movie/room-2015','admin@gmail.com','2019-06-05 07:48:45','2019-06-05 07:48:45'),(20,15,'admin@gmail.com','1559720990125-room.jpg','https://yts.am/movie/room-2015','admin@gmail.com','2019-06-05 07:49:50','2019-06-05 07:49:50'),(21,16,'admin@gmail.com','1559721160359-escaperoom.jpg','https://yts.am/movie/escape-room-2017','admin@gmail.com','2019-06-05 07:52:41','2019-06-05 07:52:41'),(22,17,'admin@gmail.com','1559721299411-medium-cover (2).jpg','https://yts.am/movie/the-room-2003','admin@gmail.com','2019-06-05 07:55:00','2019-06-05 07:55:00'),(23,18,'admin@gmail.com','1559722103754-room.jpg','https://yts.am/movie/room-2015','admin@gmail.com','2019-06-05 08:08:24','2019-06-05 08:08:24'),(24,19,'admin@gmail.com','1559722140195-room.jpg','https://yts.am/movie/room-2015','admin@gmail.com','2019-06-05 08:09:00','2019-06-05 08:09:00'),(25,19,'admin@gmail.com','1559722140195-room.jpg','https://yts.am/movie/room-2015','admin@gmail.com','2019-06-05 08:09:00','2019-06-05 08:09:00'),(26,20,'admin@gmail.com','1559731532655-terminator.jpg','https://yts.am/movie/the-terminator-1984','admin@gmail.com','2019-06-05 10:45:33','2019-06-05 10:45:33'),(27,21,'admin@gmail.com','1559900585000-btc.jpg','https://topdocumentaryfilms.com/bitcoin-beyond-bubble/','admin@gmail.com','2019-06-07 09:43:10','2019-06-07 09:43:10'),(28,22,'admin@gmail.com','1559900674334-hitler.jpg','https://topdocumentaryfilms.com/hitler-frozen-army/','admin@gmail.com','2019-06-07 09:44:35','2019-06-07 09:44:35'),(29,23,'admin@gmail.com','1559900735571-war.jpg','https://topdocumentaryfilms.com/world-war-when-aliens-attack/','admin@gmail.com','2019-06-07 09:45:36','2019-06-07 09:45:36'),(30,24,'admin@gmail.com','1559900816403-eintein.jpg','https://topdocumentaryfilms.com/einstein-quantum-riddle/','admin@gmail.com','2019-06-07 09:46:57','2019-06-07 09:46:57'),(31,25,'admin@gmail.com','1559900877311-inside-north-korea-kim-dynasty.jpg','https://topdocumentaryfilms.com/inside-north-korea-kim-dynasty/','admin@gmail.com','2019-06-07 09:47:58','2019-06-07 09:47:58'),(32,26,'admin@gmail.com','1559900949722-how-science-goes-wrong-economist-cover-19-10-2013.jpg','https://topdocumentaryfilms.com/organic-food-hype-hope/','admin@gmail.com','2019-06-07 09:49:10','2019-06-07 09:49:10'),(33,27,'admin@gmail.com','1559901030722-mega.jpg','https://topdocumentaryfilms.com/megacities-world/','admin@gmail.com','2019-06-07 09:50:31','2019-06-07 09:50:31'),(34,28,'admin@gmail.com','1559901096543-simulation.jpg','https://topdocumentaryfilms.com/simulation-hypothesis/','admin@gmail.com','2019-06-07 09:51:37','2019-06-07 09:51:37'),(35,29,'admin@gmail.com','1559901221201-how-science-goes-wrong-economist-cover-19-10-2013.jpg','https://topdocumentaryfilms.com/crisis-science/','admin@gmail.com','2019-06-07 09:53:41','2019-06-07 09:53:41'),(36,30,'admin@gmail.com','1559902702456-war.jpg','https://topdocumentaryfilms.com/world-war-when-aliens-attack/','admin@gmail.com','2019-06-07 10:18:23','2019-06-07 10:18:23'),(37,30,'admin@gmail.com','1559902702456-war.jpg','https://topdocumentaryfilms.com/world-war-when-aliens-attack/','admin@gmail.com','2019-06-07 10:18:23','2019-06-07 10:18:23'),(38,30,'admin@gmail.com','1559902702456-war.jpg','https://topdocumentaryfilms.com/world-war-when-aliens-attack/','admin@gmail.com','2019-06-07 10:18:23','2019-06-07 10:18:23'),(39,30,'admin@gmail.com','1559902702456-war.jpg','https://topdocumentaryfilms.com/world-war-when-aliens-attack/','admin@gmail.com','2019-06-07 10:18:23','2019-06-07 10:18:23'),(40,31,'admin@gmail.com','1559902757257-war.jpg','https://topdocumentaryfilms.com/world-war-when-aliens-attack/','admin@gmail.com','2019-06-07 10:19:18','2019-06-07 10:19:18'),(41,31,'admin@gmail.com','1559902757257-war.jpg','https://topdocumentaryfilms.com/world-war-when-aliens-attack/','admin@gmail.com','2019-06-07 10:19:18','2019-06-07 10:19:18'),(42,32,'admin@gmail.com','1559904900063-got.jpg','https://www.hotstar.com/tv/game-of-thrones/s-510','admin@gmail.com','2019-06-07 10:55:06','2019-06-07 10:55:06'),(43,33,'admin@gmail.com','1559905014063-got.jpg','https://www.hotstar.com/tv/game-of-thrones/s-510','admin@gmail.com','2019-06-07 10:56:54','2019-06-07 10:56:54'),(44,33,'admin@gmail.com','1559905014063-got.jpg','https://www.hotstar.com/tv/game-of-thrones/s-510','admin@gmail.com','2019-06-07 10:56:54','2019-06-07 10:56:54'),(45,34,'admin@gmail.com','1559905140752-got.jpg','https://www.hotstar.com/tv/game-of-thrones/s-510','admin@gmail.com','2019-06-07 10:59:01','2019-06-07 10:59:01'),(46,34,'admin@gmail.com','1559905140752-got.jpg','https://www.hotstar.com/tv/game-of-thrones/s-510','admin@gmail.com','2019-06-07 10:59:01','2019-06-07 10:59:01'),(47,34,'admin@gmail.com','1559905140752-got.jpg','https://www.hotstar.com/tv/game-of-thrones/s-510','admin@gmail.com','2019-06-07 10:59:01','2019-06-07 10:59:01'),(48,35,'admin@gmail.com','1559905203687-got.jpg','https://www.hotstar.com/tv/game-of-thrones/s-510','admin@gmail.com','2019-06-07 11:00:04','2019-06-07 11:00:04'),(49,36,'admin@gmail.com','1559905337601-got.jpg','https://www.hotstar.com/tv/game-of-thrones/s-510','admin@gmail.com','2019-06-07 11:02:18','2019-06-07 11:02:18'),(50,36,'admin@gmail.com','1559905337601-got.jpg','https://www.hotstar.com/tv/game-of-thrones/s-510','admin@gmail.com','2019-06-07 11:02:18','2019-06-07 11:02:18'),(51,37,'admin@gmail.com','1559905387700-got.jpg','https://www.hotstar.com/tv/game-of-thrones/s-510','admin@gmail.com','2019-06-07 11:03:08','2019-06-07 11:03:08'),(52,37,'admin@gmail.com','1559905387700-got.jpg','https://www.hotstar.com/tv/game-of-thrones/s-510','admin@gmail.com','2019-06-07 11:03:08','2019-06-07 11:03:08'),(53,37,'admin@gmail.com','1559905387700-got.jpg','https://www.hotstar.com/tv/game-of-thrones/s-510','admin@gmail.com','2019-06-07 11:03:08','2019-06-07 11:03:08'),(54,37,'admin@gmail.com','1559905387700-got.jpg','https://www.hotstar.com/tv/game-of-thrones/s-510','admin@gmail.com','2019-06-07 11:03:08','2019-06-07 11:03:08'),(55,38,'admin@gmail.com','1559905492814-flash.jpg','https://www.mediatvmovies.online/tv/60735-5-1/the-flash.html','admin@gmail.com','2019-06-07 11:04:53','2019-06-07 11:04:53'),(56,38,'admin@gmail.com','1559905492814-flash.jpg','https://www.mediatvmovies.online/tv/60735-5-1/the-flash.html','admin@gmail.com','2019-06-07 11:04:53','2019-06-07 11:04:53'),(57,39,'admin@gmail.com','1559905544096-flash.jpg','https://www.mediatvmovies.online/tv/60735-5-1/the-flash.html','admin@gmail.com','2019-06-07 11:05:45','2019-06-07 11:05:45'),(58,40,'admin@gmail.com','1559905647172-riverdale.jpg','http://mediatvmovies.online/tv/69050-3/riverdale.html','admin@gmail.com','2019-06-07 11:07:27','2019-06-07 11:07:27'),(59,40,'admin@gmail.com','1559905647172-riverdale.jpg','http://mediatvmovies.online/tv/69050-3/riverdale.html','admin@gmail.com','2019-06-07 11:07:27','2019-06-07 11:07:27'),(60,40,'admin@gmail.com','1559905647172-riverdale.jpg','http://mediatvmovies.online/tv/69050-3/riverdale.html','admin@gmail.com','2019-06-07 11:07:27','2019-06-07 11:07:27'),(61,41,'sridhar@gmail.com','1559992794999-medium-cover (3).jpg','https://yts.lt/movie/hotel-mumbai-2018','sridhar@gmail.com','2019-06-08 11:19:57','2019-06-08 11:19:57');
/*!40000 ALTER TABLE `show_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shows`
--

DROP TABLE IF EXISTS `shows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shows` (
  `show_id` int(11) NOT NULL AUTO_INCREMENT,
  `showType` varchar(250) NOT NULL,
  `trailerType` varchar(100) NOT NULL,
  `language` varchar(250) NOT NULL,
  `genre` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `singer` varchar(250) NOT NULL,
  `partOrSeason` varchar(250) NOT NULL,
  `episode` varchar(250) NOT NULL,
  `requested_by` varchar(250) NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`show_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shows`
--

LOCK TABLES `shows` WRITE;
/*!40000 ALTER TABLE `shows` DISABLE KEYS */;
INSERT INTO `shows` VALUES (1,'movie','','English','Drama, Thriller','Room','','2015','','','admin@gmail.com','2019-06-04 06:29:48','2019-06-04 06:29:48'),(2,'movie','','English','Action, Sci-fi','Captain Marvel','','2019','','','admin@gmail.com','2019-06-04 07:22:29','2019-06-04 07:22:29'),(3,'movie','','English','Action','How to Train You Dragon','','The hidden world','','','admin@gmail.com','2019-06-04 07:26:36','2019-06-04 07:26:36'),(4,'movie','','English','Action, Thriller, Fantasy','Mortal Engine','','2018','','','admin@gmail.com','2019-06-04 07:28:12','2019-06-04 07:28:12'),(5,'movie','','English','Action, Sci-fi','Terminator','','1984','','','admin@gmail.com','2019-06-04 07:29:29','2019-06-04 07:29:29'),(6,'movie','','English','Action, Sci-fi','Terminator','','Judgement Day','','','admin@gmail.com','2019-06-04 07:30:50','2019-06-04 07:30:50'),(7,'movie','','English','Action, Sci-fi','Terminator','','Rise of the machines','','','admin@gmail.com','2019-06-04 07:32:29','2019-06-04 07:32:29'),(8,'movie','','English','Action, Sci-fi','Terminator','','Genisys','','','admin@gmail.com','2019-06-04 07:33:43','2019-06-04 07:33:43'),(9,'movie','','English','Drama, Fantasy, Adventure','Harry Potter','','Sorcerer\'s Stone','','','admin@gmail.com','2019-06-04 07:35:02','2019-06-04 07:35:02'),(10,'movie','','English','Adventure, Fantasy','Harry Potter','','The Chamber of Secret','','','admin@gmail.com','2019-06-04 07:36:34','2019-06-04 07:36:34'),(11,'movie','','English','Action, Sci-fi','Captain Marvel','','2019','','','admin@gmail.com','2019-06-04 12:19:20','2019-06-04 12:19:20'),(12,'movie','','English','Action, Sci-fi','Captain Marvel','','2019','','','admin@gmail.com','2019-06-04 12:20:04','2019-06-04 12:20:04'),(13,'movie','','English','Thriller, Drama','Room Brie Larson','','2015','','','admin@gmail.com','2019-06-05 07:47:29','2019-06-05 07:47:29'),(14,'movie','','Enlish','Thriller, Drama','Movie Room','','2015','','','admin@gmail.com','2019-06-05 07:48:45','2019-06-05 07:48:45'),(15,'movie','','English','Drama, Thriller','movie room movie','','2015','','','admin@gmail.com','2019-06-05 07:49:50','2019-06-05 07:49:50'),(16,'movie','','Enlish','Horrer','Escape Room','','2017','','','admin@gmail.com','2019-06-05 07:52:41','2019-06-05 07:52:41'),(17,'movie','','English','Action, comedy, drama','The Room','','2003','','','admin@gmail.com','2019-06-05 07:55:00','2019-06-05 07:55:00'),(18,'movie','','English','Thriller','room','','2015','','','admin@gmail.com','2019-06-05 08:08:24','2019-06-05 08:08:24'),(19,'movie','','English','Drama','room','','2015','','','admin@gmail.com','2019-06-05 08:09:00','2019-06-05 08:09:00'),(20,'movie','','English','Action, Sci-fi','Terminator','','Judgement Day','','','admin@gmail.com','2019-06-05 10:45:33','2019-06-05 10:45:33'),(21,'documentary','','English','Technology','Bitcoin','','Beyond the bubble','','','admin@gmail.com','2019-06-07 09:43:08','2019-06-07 09:43:08'),(22,'documentary','','English','History','Hitler\'s Frozen Army','','2013','','','admin@gmail.com','2019-06-07 09:44:34','2019-06-07 09:44:34'),(23,'documentary','','English','Mystory','World War A','','When Aliens Attack','','','admin@gmail.com','2019-06-07 09:45:36','2019-06-07 09:45:36'),(24,'documentary','','English','Science','Einstein\'s Quantum Riddle','','2019','','','admin@gmail.com','2019-06-07 09:46:57','2019-06-07 09:46:57'),(25,'documentary','','English','Political','Inside North Korea','','The Kim Dynasty','','','admin@gmail.com','2019-06-07 09:47:58','2019-06-07 09:47:58'),(26,'documentary','','Enlish','Health','Organic Food','','Hype or Hope?','','','admin@gmail.com','2019-06-07 09:49:10','2019-06-07 09:49:10'),(27,'documentary','','English','Society','Megacities of the World','','2019','','','admin@gmail.com','2019-06-07 09:50:31','2019-06-07 09:50:31'),(28,'documentary','','English','Philosophy','The Simulation Hypothesis','','2015','','','admin@gmail.com','2019-06-07 09:51:37','2019-06-07 09:51:37'),(29,'documentary','','English','Conspirasy','The Crisis of Science','','2019','','','admin@gmail.com','2019-06-07 09:53:41','2019-06-07 09:53:41'),(30,'documentary','','English','Mystery','World War A','','When Aliens Attack','','','admin@gmail.com','2019-06-07 10:18:23','2019-06-07 10:18:23'),(31,'documentary','','English','Mystery','World War A','','When Aliens Attack','','','admin@gmail.com','2019-06-07 10:19:17','2019-06-07 10:19:17'),(32,'tv_show','','English','Action, Thriller, fantasy','Game of Thrones','','se1','ep1','','admin@gmail.com','2019-06-07 10:55:06','2019-06-07 10:55:06'),(33,'tv_show','','English','Action','Game of thrones','','se1','ep2','','admin@gmail.com','2019-06-07 10:56:54','2019-06-07 10:56:54'),(34,'tv_show','','English','Thriller','Game of Thrones','','se2','ep3','','admin@gmail.com','2019-06-07 10:59:01','2019-06-07 10:59:01'),(35,'tv_show','','English','Action','Game of thrones','','se5','ep8','','admin@gmail.com','2019-06-07 11:00:04','2019-06-07 11:00:04'),(36,'tv_show','','English','Fantasy','Game of Thrones','','se1','ep1','','admin@gmail.com','2019-06-07 11:02:18','2019-06-07 11:02:18'),(37,'tv_show','','English','Fantasy','Game of Thrones','','se2','ep3','','admin@gmail.com','2019-06-07 11:03:08','2019-06-07 11:03:08'),(38,'tv_show','','English','Sci-fi, Action','The Flash','','se3','ep2','','admin@gmail.com','2019-06-07 11:04:53','2019-06-07 11:04:53'),(39,'tv_show','','English','Sci-fi','flash','','se3','ep1','','admin@gmail.com','2019-06-07 11:05:45','2019-06-07 11:05:45'),(40,'tv_show','','English','Mystery, Drama','Riverdale','','se2','ep8','','admin@gmail.com','2019-06-07 11:07:27','2019-06-07 11:07:27'),(41,'movie','','English','History, Thriller, Drama','Hotel Mumbai','','2018','','','sridhar@gmail.com','2019-06-08 11:19:56','2019-06-08 11:19:56');
/*!40000 ALTER TABLE `shows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(200) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin@gmail.com','$2b$10$jSNfZBe.755ps4H21RXIl.AoWN.mQV3u91JbHkNKzLOkOaaGKLLF2','2019-06-03 06:47:23','2019-06-03 06:47:23'),(5,'sridhar','sridhar@gmail.com','$2b$10$Zt0MIK7ZzNOivCecANWuCu3EKtpTAnq9kq9cFx/n/z4FXGhGLMbcG','2019-06-08 11:05:19','2019-06-08 11:05:19');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-08 17:29:10
