-- MySQL dump 10.16  Distrib 10.1.37-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: movie_linker
-- ------------------------------------------------------
-- Server version	10.1.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `shows`
--

DROP TABLE IF EXISTS `shows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shows` (
  `show_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` varchar(50) NOT NULL,
  `answer_id` varchar(50) NOT NULL,
  `showType` varchar(250) NOT NULL,
  `trailerType` varchar(100) NOT NULL,
  `language` varchar(250) NOT NULL,
  `genre` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `singer` varchar(250) NOT NULL,
  `partOrSeason` varchar(250) NOT NULL,
  `episode` varchar(250) NOT NULL,
  `image_name` varchar(250) NOT NULL,
  `youtubeLinks` varchar(250) NOT NULL,
  `links` varchar(250) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_on` date NOT NULL,
  `updated_on` date NOT NULL,
  PRIMARY KEY (`show_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shows`
--

LOCK TABLES `shows` WRITE;
/*!40000 ALTER TABLE `shows` DISABLE KEYS */;
INSERT INTO `shows` VALUES (1,'','','movie','','Language','Action','How to Train Your Dragon','','The Hidden World','','medium-cover.jpg','','https://yts.am/movie/how-to-train-your-dragon-the-hidden-world-2019','','0000-00-00','0000-00-00'),(2,'','','tv_show','','English','Action, Thriller, Fantasy','Game of Thrones','','Season 7','Episode 1','got.jpg','','https://www.hotstar.com/tv/game-of-thrones/s-510','','0000-00-00','0000-00-00'),(3,'','','movie','','English','Action, Thriller, Fantsay','Mortal Engine','','Part 1','','medium-cover (1).jpg','','https://yts.am/movie/mortal-engines-2018','','0000-00-00','0000-00-00'),(4,'','','movie','','English','Action, Sci - Fi','Terminator','','1984','','terminator.jpg','','https://yts.am/movie/the-terminator-1984','','0000-00-00','0000-00-00'),(5,'','','movie','','English','Action, Sci - Fi','Terminator 2','','Judgement Day','','terminator.jpg','','https://yts.am/movie/terminator-2-judgment-day-1991','','0000-00-00','0000-00-00'),(6,'','','movie','','English','Action, Sci - fi','Terminator 3','','Rise of the machines','','terminator.jpg','','https://yts.am/movie/terminator-3-rise-of-the-machines-2003','','0000-00-00','0000-00-00'),(7,'','','movie','','English','Action, Sci - fi','Terminator 4','','Genisys','','terminator.jpg','','https://yts.am/movie/terminator-genisys-2015','','0000-00-00','0000-00-00'),(8,'','','movie','','English','Fantasty, Adventure','Harry Potter','','Sorcerer\'s Stone','','harry-potter.jpg','','https://yts.am/movie/harry-potter-and-the-sorcerers-stone-2001','','0000-00-00','0000-00-00'),(9,'','','movie','','English','Fantasy, Adventure','Harry Potter','','The Chamber of Secrets','','harry-potter.jpg','','https://yts.am/movie/harry-potter-and-the-chamber-of-secrets-2002','','0000-00-00','0000-00-00'),(10,'','','tv_show','','English','Drama,Sci-fi,Mystery','Stranger Things','','Season 1','Episode 1','stranger-things.jpg','','https://snipetv.com/stranger-things','','0000-00-00','0000-00-00'),(11,'','','tv_show','','English','Dram, Mystery, Sci-fi','stranger things','','Season1','Episode2','stranger-things.jpg','','https://snipetv.com/stranger-things/01x02','','0000-00-00','0000-00-00'),(12,'','','tv_show','','English','drama,sci-fi,mystery','Stranger Things','','season 1','episode 3','stranger-things.jpg','','https://snipetv.com/stranger-things/01x03','','0000-00-00','0000-00-00'),(13,'','','tv_show','','english','drama,mystery','stranger things','','season1','episode4','stranger-things.jpg','','https://snipetv.com/stranger-things/01x04','','0000-00-00','0000-00-00'),(14,'','','tv_show','','english','drama, sci-fi','stranger things','','se01','ep5','stranger-things.jpg','','https://snipetv.com/stranger-things/01x05','','0000-00-00','0000-00-00'),(15,'','','tv_show','','english','drama, mystery','stranger things','','se01','ep06','stranger-things.jpg','','https://snipetv.com/stranger-things/01x06','','0000-00-00','0000-00-00'),(16,'','','tv_show','','english','sci-fi, mystery','stranger things','','se01','ep7','stranger-things.jpg','','https://snipetv.com/stranger-things/01x07','','0000-00-00','0000-00-00'),(17,'','','tv_show','','English','Drama','Stranger Things','','se02','ep01','stranger-things.jpg','','https://snipetv.com/stranger-things/01x03','','0000-00-00','0000-00-00'),(18,'','','tv_show','','englsih','drama','stranger things','','se02','ep02','stranger-things.jpg','','https://snipetv.com/stranger-things/02x02','','0000-00-00','0000-00-00'),(19,'','','songs','','English','Pop','Blank Space','Taylor Swift','','','','https://www.youtube.com/watch?v=e-ORhEE9VVg','','','0000-00-00','0000-00-00'),(20,'','','songs','','English','Pop','Shape of You','Ed sheeran','','','','https://www.youtube.com/watch?v=JGwWNGJdvx8','','','0000-00-00','0000-00-00'),(21,'','','songs','','Tamil','Folk','Kanaa - Othaiyadi Pathayila','Anirudh Ravichandar','','','','https://www.youtube.com/watch?v=qP8e7lFdEho','','','0000-00-00','0000-00-00'),(22,'','','songs','','Hindi','Party','Badtameez Dil','','','','','https://www.youtube.com/watch?v=II2EO3Nw4m0','','','0000-00-00','0000-00-00'),(23,'','','songs','','English','Party','Maroon 5 - Sugar','Maroon 5','','','','https://www.youtube.com/watch?v=09R8_2nJtjg','','','0000-00-00','0000-00-00'),(24,'','','songs','','English','Pop','Baby ft Ludacris','Justin Bieber','','','','https://www.youtube.com/watch?v=kffacxfA7G4','','','0000-00-00','0000-00-00'),(25,'','','songs','','English','Pop','Never Say Never','Justin Bieber','','','','https://www.youtube.com/watch?v=BblYQnc9tL8','','','0000-00-00','0000-00-00'),(26,'','','movie','','English','Action, Fantasy','The Lego Movie 2','','The Second Part','','lego2.jpg','','https://yts.am/movie/the-lego-movie-2-the-second-part-2019','','0000-00-00','0000-00-00'),(27,'','','songs','','English','Melody','Faded','Alan Walker','','','','https://www.youtube.com/watch?v=60ItHLz5WEA','','','0000-00-00','0000-00-00'),(28,'','','songs','','Tamil','Rock','Rowdy Baby - Maari 2','Dhanush, Dhee','','','rowdybaby.jpg','','https://www.hungama.com/video/rowdy-baby/45552911/','','0000-00-00','0000-00-00'),(29,'','','songs','','English','Pop','Girls Like You','','','','maroon5.jpg','','https://www.hungama.com/video/girls-like-you/36500632/','','0000-00-00','0000-00-00'),(30,'','','trailer','movie','English','Action, Thriller','Gemini Man','','2019','','','https://www.youtube.com/watch?v=o8-YD9dnqZ8','','','0000-00-00','0000-00-00'),(31,'','','trailer','movie','English','Action, Thriller','Avengers','','Endgame','','','https://www.youtube.com/watch?v=JyGGLB542ks','','','0000-00-00','0000-00-00'),(32,'','','trailer','movie','Tamil','Action','Kaappaan','','','','kaapan.jpg','','https://www.indiaglitz.com/kaappaan-tamil-movie-trailers-22320','','0000-00-00','0000-00-00'),(33,'','','trailer','tv_show','English','Action, Thriller, Adventure','Game of Thrones','','Season 8','ep3','','https://www.youtube.com/watch?v=TdkS4Xazz7Q','','','0000-00-00','0000-00-00'),(34,'','','trailer','tv_show','English','Action, Sci-fi','The Flash','','Season5','ep18','','https://www.youtube.com/watch?v=9WQ3Veypb7s','','','0000-00-00','0000-00-00'),(35,'','','trailer','documentary','English','Political','The Brik','','2019','','','https://www.youtube.com/watch?v=9WQ3Veypb7s','','','0000-00-00','0000-00-00'),(36,'','','trailer','documentary','English','War','The Complete History of the Second World War','','','','complete-history-second-world-war-150x198.jpg','','https://topdocumentaryfilms.com/complete-history-second-world-war/','','0000-00-00','0000-00-00'),(37,'','','trailer','anime','Japanese','Action','Code Geass','',' Lelouch of the Resurrection (2019)','','','https://www.youtube.com/watch?v=hk2d8CG4kdQ','','','0000-00-00','0000-00-00'),(38,'','','trailer','anime','Japanese','Fantasy','The Rising of the Shield Hero','','season1','ep16','the-rising-of-the-shield-hero.jpg','','https://manga.tokyo/news/the-rising-of-the-shield-hero-episode-16-preview-stills-and-synopsis/','','0000-00-00','0000-00-00'),(39,'','','documentary','','English','Technology','Bitcoin','','Beyond the Bubble','','btc.jpg','','https://topdocumentaryfilms.com/bitcoin-beyond-bubble/','','0000-00-00','0000-00-00'),(40,'','','documentary','','English','History','Hitler\'s Frozen Army','','','','hitler.jpg','','https://topdocumentaryfilms.com/hitler-frozen-army/','','0000-00-00','0000-00-00'),(41,'','','documentary','','English','Mystery','World War A','','When Aliens Attack','','war.jpg','','https://topdocumentaryfilms.com/world-war-when-aliens-attack/','','0000-00-00','0000-00-00'),(42,'','','documentary','','English','science','Einstein\'s Quantum Riddle','','','','eintein.jpg','','https://topdocumentaryfilms.com/einstein-quantum-riddle/','','0000-00-00','0000-00-00'),(43,'','','documentary','','English','politics','Inside North Korea','','The Kim Dynasty','','inside-north-korea-kim-dynasty.jpg','','https://topdocumentaryfilms.com/inside-north-korea-kim-dynasty/','','0000-00-00','0000-00-00'),(44,'','','documentary','','English','health','Organic Food','','Hype or Hope?','','organic-food-hype-hope.jpg','','https://topdocumentaryfilms.com/organic-food-hype-hope/','','0000-00-00','0000-00-00'),(45,'','','documentary','','English','soceity','Megacities of the World','','','','mega.jpg','','https://topdocumentaryfilms.com/megacities-world/','','0000-00-00','0000-00-00'),(46,'','','documentary','','English','philosophy','The Simulation Hypothesis','','','','simulation.jpg','','https://topdocumentaryfilms.com/simulation-hypothesis/','','0000-00-00','0000-00-00'),(47,'','','documentary','','English','conspiracy','The Crisis of Science','','','','how-science-goes-wrong-economist-cover-19-10-2013.jpg','','https://topdocumentaryfilms.com/crisis-science/','','0000-00-00','0000-00-00'),(48,'','','tv_show','','English','Action, sci-fi','The Flash','','Season05','ep01','flash.jpg','','https://www.mediatvmovies.online/tv/60735-5-1/the-flash.html','','0000-00-00','0000-00-00'),(49,'','','tv_show','','Englsih','Action, Sci-fi','The Flash','','Season05','episode02','flash.jpg','','https://www.mediatvmovies.online/tv/60735-5-1/the-flash.html','','0000-00-00','0000-00-00'),(50,'','','tv_show','','English','Drama, Mystery','Riverdale','','season03','ep01','riverdale.jpg','','http://mediatvmovies.online/tv/69050-3/riverdale.html','','0000-00-00','0000-00-00');
/*!40000 ALTER TABLE `shows` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-29 17:17:53
